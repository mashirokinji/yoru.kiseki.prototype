﻿using UnityEngine;

namespace YoRu.Unity
{
	public static class MonoBehaviourEx
	{
		public static TComponent GetAddComponent<TComponent>(this MonoBehaviour monoBehaviour)
			where TComponent : Component
		{
			return monoBehaviour.GetComponent<TComponent>() ??
				monoBehaviour.gameObject.AddComponent<TComponent>();
		}
	}
}
