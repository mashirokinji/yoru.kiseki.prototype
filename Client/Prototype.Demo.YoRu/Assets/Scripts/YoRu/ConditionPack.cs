﻿using System;
using System.Collections.Generic;

namespace YoRu
{
	public sealed class ConditionPack
	{
		public bool isMeet
		{
			get
			{
				foreach (var condition in m_Conditions)
				{
					if (condition == null) continue;
					if (!condition()) return false;
				}
				return true;
			}
		}

		public void AddCondition(Func<bool> aCondition)
		{
			if (aCondition == null) return;
			//if (m_Conditions.Contains(aCondition)) return;
			m_Conditions.Add(aCondition);
		}

		public bool RemoveCondition(Func<bool> aCondition)
		{
			if (aCondition == null) return false;
			//if (!m_Conditions.Contains(aCondition)) return false;
			return m_Conditions.Remove(aCondition);
		}

		private readonly List<Func<bool>> m_Conditions = new List<Func<bool>>();
	}
}

