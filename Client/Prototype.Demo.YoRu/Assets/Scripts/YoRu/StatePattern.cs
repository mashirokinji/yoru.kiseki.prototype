namespace YoRu
{
	public class StatePattern<T> where T : StatePattern<T>.IState
	{
		public interface IState {}

		public IState current { get; set; }
	}
}
