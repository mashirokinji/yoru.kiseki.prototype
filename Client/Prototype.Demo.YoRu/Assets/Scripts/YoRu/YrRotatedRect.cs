﻿using UnityEngine;
using System;

namespace YoRu
{
	public struct YrRotatedRect
	{
		public YrRotatedRect(Rect aRect, float aAngle)
		{
			m_Rect = aRect;
			angle = aAngle;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:YrRotatedRect"/> struct.
		/// </summary>
		/// <param name="aLine">A line.</param>
		/// <param name="pld">Distance of parallel line of <paramref name="aLine"/></param>
		public YrRotatedRect(YrLine aLine, float pld)
		{
			throw new NotImplementedException();
		}

		public bool Contain(YrCircle circle)
		{
			var rx = x;
			var ry = y;
			var cx = circle.x;
			var cy = circle.y;

			// get new position of circle after rotate canvas
			var cx1 = Mathf.Cos(-angle) * (cx - rx) - Mathf.Sin(-angle) * (cy - ry) + rx;
			var cy1 = Mathf.Sin(-angle) * (cx - rx) - Mathf.Cos(-angle) * (cy - ry) + ry;

			return m_Rect.Contain(new YrCircle(cx1, cy1, circle.radius));
		}

		public float x
		{
			get { return center.x; }
			set
			{
				var c = center;
				center = new Vector2(value, c.y);
			}
		}

		public float y
		{
			get { return center.y; }
			set
			{
				var c = center;
				center = new Vector2(value, c.y);
			}
		}

		//public Vector2 size { get { return m_Rect.size; } set { m_Rect.size = value; } }
		public Vector2 center { get { return m_Rect.center; } set { m_Rect.center = value; } }
		public float angle { get; set; }

		private Rect m_Rect;
	}
}