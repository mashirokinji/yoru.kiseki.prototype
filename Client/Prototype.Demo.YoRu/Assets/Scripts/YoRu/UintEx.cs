﻿namespace YoRu
{
	public static class UintEx
	{
		public static uint Square(this uint value)
		{
			return value * value;
		}
	}
}
