﻿using System.Collections;
using System.Collections.Generic;

namespace YoRu
{
	public struct ReadOnlyDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
	{
		public ReadOnlyDictionary(Dictionary<TKey, TValue> aDictionary)
		{
			m_Dictionary = aDictionary;
		}

		public struct Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>
		{
			public Enumerator(ReadOnlyDictionary<TKey, TValue> aReadOnlyDictionary)
			{
				m_ReadOnlyDictionary = aReadOnlyDictionary;
				m_Enumerator = aReadOnlyDictionary.m_Dictionary.GetEnumerator();
			}

			public void Dispose() => m_Enumerator.Dispose();

			public bool MoveNext() => m_Enumerator.MoveNext();

			public KeyValuePair<TKey, TValue> Current => m_Enumerator.Current;

			public bool isVaild => m_ReadOnlyDictionary != null;

			private readonly ReadOnlyDictionary<TKey, TValue>? m_ReadOnlyDictionary;
			private Dictionary<TKey, TValue>.Enumerator m_Enumerator;

			void IEnumerator.Reset() => ((IEnumerator)m_Enumerator).Reset();

			object IEnumerator.Current => Current;
		}

		public TValue this[TKey aKey] => m_Dictionary[aKey];

		public Enumerator GetEnumerator() => new Enumerator(this);

		public bool ContainsKey(TKey aKey) => m_Dictionary.ContainsKey(aKey);

		public bool ContainsValue(TKey aKey) => m_Dictionary.ContainsKey(aKey);

		public bool TryGetValue(TKey aKey, out TValue aValue) => m_Dictionary.TryGetValue(aKey, out aValue);

		public int Count => m_Dictionary.Count;

		public bool isVaild => m_Dictionary != null;

		private readonly Dictionary<TKey, TValue> m_Dictionary;

		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() => GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
