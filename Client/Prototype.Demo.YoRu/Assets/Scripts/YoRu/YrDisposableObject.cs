﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace YoRu
{
	public class YrDisposableObject : IDisposable
	{
		public YrDisposableObject()
		{
			ALL.Add(this);
			onAdd?.Invoke(this);
		}

		~YrDisposableObject()
		{
			if (m_HasDispose) return;
			Dispose();
		}

		public struct Enumerator : IEnumerator<YrDisposableObject>
		{
			public bool MoveNext()
			{
				if (!m_HasInit) Init();
				return m_Enumerator.MoveNext();
			}

			public void Reset()
			{
				if (!m_HasInit) Init();
			}

			public void Dispose()
			{
			}

			public YrDisposableObject Current => m_Enumerator.Current;

			private void Init()
			{
				m_Enumerator = ALL.GetEnumerator();
				m_HasInit = true;
			}

			private bool m_HasInit;
			private List<YrDisposableObject>.Enumerator m_Enumerator;

			object IEnumerator.Current => Current;
		}

		public static event Action<YrDisposableObject> onAdd;
		public static event Action<YrDisposableObject> onRemove;

		public void Dispose()
		{
			ALL.Remove(this);
			onRemove?.Invoke(this);
			m_HasDispose = true;
		}

		private static readonly List<YrDisposableObject> ALL = new List<YrDisposableObject>();
		private bool m_HasDispose;
	}
}
