﻿using System.Collections.Generic;
using UnityEngine;

namespace YoRu
{
	public struct YrLine
	{
		public Vector2 p1 { get => m_P1; set => m_P1 = value; }
		public Vector2 p2 { get => m_P2; set => m_P2 = value; }
		public float x1 { get => m_P1.x; set => m_P1.x = value; }
		public float x2 { get => m_P2.x; set => m_P2.x = value; }
		public float y1 { get => m_P1.y; set => m_P1.y = value; }
		public float y2 { get => m_P2.y; set => m_P2.y = value; }

		private Vector2 m_P1;
		private Vector2 m_P2;
	}

	public static class IListEx
	{
		public static T Last<T>(this IList<T> array)
		{
			return array[array.Count - 1];
		}
	}
}