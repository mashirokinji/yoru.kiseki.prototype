﻿using System;

namespace YoRu
{
	public abstract class Singleton<T> where T : Singleton<T>, new()
	{
		static Singleton()
		{
			INSTANCE = new T();
		}

		protected Singleton()
		{
			if (!GetType().IsSealed) throw new Exception("The class assignable from Singletion<T> should be sealed");
			if (!(this is T)) throw new Exception("Generic T must be " + GetType().Name);
			if (INSTANCE != null)
			{
				throw new Exception("Don't create " + GetType().Name + ", please use " + GetType().Name + ".instance");
			}
		}

		public static T instance { get { return INSTANCE; } }
		private static readonly T INSTANCE;
	}
}
