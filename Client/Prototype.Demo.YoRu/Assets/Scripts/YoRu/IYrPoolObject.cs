namespace YoRu
{
	public interface IYrPoolObject
	{
		void Init();
		void Release();
		void Reset();
	}

	public interface IYrPoolObject<TParam>
	{
		void Init(TParam aParam);
		void Release();
		void Reset();
	}
}