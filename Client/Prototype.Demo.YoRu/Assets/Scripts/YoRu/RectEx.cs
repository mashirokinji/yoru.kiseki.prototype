﻿using UnityEngine;

namespace YoRu
{
	public static class RectEx
	{
		public static Vector2 FindNeasetPos(this Rect aRect, Vector2 aPos)
		{
			var px = aPos.x;
			return new Vector2(
				Mathf.Clamp(aPos.x, aRect.xMin, aRect.xMax),
				Mathf.Clamp(aPos.y, aRect.yMin, aRect.yMax));
		}

		public static bool Contain(this Rect aRect, YrCircle aCircle)
		{
			var np = aRect.FindNeasetPos(aCircle.center);
			var sm = (aCircle.center - np).sqrMagnitude;
			return sm <= aCircle.radius.Square();
		}
	}
}