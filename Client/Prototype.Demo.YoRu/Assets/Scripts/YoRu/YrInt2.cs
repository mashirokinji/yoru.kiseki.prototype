﻿namespace YoRu
{ 
	public struct YrInt2
	{
		public int x { get; set; }
		public int y { get; set; }
	}
}