using System.Collections;
using System.Collections.Generic;

namespace YoRu
{
	public struct ReadOnlyList<TValue> : IEnumerable<TValue>
	{
		public ReadOnlyList(List<TValue> aList)
		{
			m_List = aList;
		}

		public struct Enumerator : IEnumerator<TValue>
		{
			public Enumerator(ReadOnlyList<TValue> aReadOnlyList)
			{
				m_ReadOnlyDictionary = aReadOnlyList;
				m_Enumerator = aReadOnlyList.m_List.GetEnumerator();
			}

			public void Dispose() => m_Enumerator.Dispose();

			public bool MoveNext() => m_Enumerator.MoveNext();

			public TValue Current => m_Enumerator.Current;

			public bool isVaild => m_ReadOnlyDictionary != null;

			private readonly ReadOnlyList<TValue>? m_ReadOnlyDictionary;
			private List<TValue>.Enumerator m_Enumerator;

			void IEnumerator.Reset() => ((IEnumerator)m_Enumerator).Reset();

			object IEnumerator.Current => Current;
		}

		public TValue this[int aIndex] => m_List[aIndex];

		public Enumerator GetEnumerator() => new Enumerator(this);

		public int Count => m_List.Count;

		public bool isVaild => m_List != null;

		private readonly List<TValue> m_List;

		IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator() => GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
