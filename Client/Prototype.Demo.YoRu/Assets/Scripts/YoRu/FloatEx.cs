﻿namespace YoRu
{
	public static class FloatEx
	{
		// Returns second power of value
		public static float Square(this float aValue)
		{
			return aValue * aValue;
		}

		// Returns square root of value
		public static float Root(this float aValue)
		{
			return UnityEngine.Mathf.Sqrt(aValue);
		}
	}
}
