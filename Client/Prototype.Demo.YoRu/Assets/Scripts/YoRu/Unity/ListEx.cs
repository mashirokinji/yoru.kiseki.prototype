﻿using System.Collections.Generic;

namespace YoRu.Unity
{
	public static class ListEx
	{
		public static bool TryGet<T>(this List<T> aList, int aIndex, out T aValue)
		{
			aValue = default;
			if (aList == null) return false;
			if (aIndex < 0 || aIndex >= aList.Count) return false;
			aValue = aList[aIndex];
			return true;
		}

		public static T Last<T>(this List<T> aList)
		{
			var a = new int[10];
			return aList[aList.Count];
		}
	}
}
