﻿using UnityEngine;
using UnityEngine.Events;
using System;

namespace YoRu.Unity
{
	[Serializable]
	public abstract class BaseAxesButton : YrObject
	{
		public event Action onClick;
		public event Action onPress;
		public event Action onButtonUp;
		public event Action onButtonDown;

		public abstract string axesName { get; }

		public void Update()
		{
			bool isButtonPress = Input.GetButton(axesName);
			if (!isButtonPress && m_IsButtonPress)
			{
				onClick?.Invoke();
				m_OnClick?.Invoke();
			}
			if (isButtonPress)
			{
				onPress?.Invoke();
				m_OnPress?.Invoke();
			}
			if (Input.GetButtonUp(axesName))
			{
				onButtonUp?.Invoke();
				m_OnButtonUp?.Invoke();
			}
			if (Input.GetButtonDown(axesName))
			{
				onButtonDown?.Invoke();
				m_OnButtonDown?.Invoke();
			}
			m_IsButtonPress = isButtonPress;
		}

		private bool m_IsButtonPress;

#pragma warning disable 0649
		[SerializeField] private UnityEvent m_OnClick;
		[SerializeField] private UnityEvent m_OnPress;
		[SerializeField] private UnityEvent m_OnButtonUp;
		[SerializeField] private UnityEvent m_OnButtonDown;
#pragma warning restore 0649
	}
}