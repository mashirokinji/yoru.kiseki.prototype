﻿using UnityEngine;

namespace YoRu.Unity.Mono
{
	[CreateAssetMenu(menuName = AnimatorAction.CREATE_ASSET_MENU_PATH + CREATE_ASSET_MENU_PATH)]
	public sealed class AnimatorAction_Play_StateName_Layer_NormalizedTime : AnimatorAction
	{
		public new const string CREATE_ASSET_MENU_PATH = "Play(stateName, layer, normalizedTime)";

		public override void Do(Animator aAnimator)
		{
			aAnimator.Play(m_StateName, m_Layer, m_NormalizedTime);
		}

#pragma warning disable 0649
		[SerializeField] private string m_StateName;
		[SerializeField] private int m_Layer;
		[SerializeField] private float m_NormalizedTime;
#pragma warning restore 0649
	}
}