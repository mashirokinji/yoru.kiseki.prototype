﻿using UnityEngine;

namespace YoRu.Unity.Mono
{
	[CreateAssetMenu(menuName = AnimatorAction.CREATE_ASSET_MENU_PATH + CREATE_ASSET_MENU_PATH)]
	public sealed class AnimatorAction_SetInteger_Name_Value : AnimatorAction
	{
		public new const string CREATE_ASSET_MENU_PATH = "SetInterger(name, value)";

		public override void Do(Animator aAnimator)
		{
			aAnimator.SetInteger(m_StateName, m_Value);
		}

#pragma warning disable 0649
		[SerializeField] private string m_StateName;
		[SerializeField] private int m_Value;
#pragma warning restore 0649
	}
}