﻿using UnityEngine;

namespace YoRu.Unity.Mono
{
	[CreateAssetMenu(menuName = AnimatorAction.CREATE_ASSET_MENU_PATH + CREATE_ASSET_MENU_PATH)]
	public sealed class AnimatorAction_PlayEnd_StateName_Layer : AnimatorAction
	{
		public new const string CREATE_ASSET_MENU_PATH = "PlayEnd(stateName, layer)";

		public override void Do(Animator aAnimator)
		{
			aAnimator.Play(m_StateName, m_Layer, 1);
		}

#pragma warning disable 0649
		[SerializeField] private string m_StateName;
		[SerializeField] private int m_Layer;
#pragma warning restore 0649
	}
}