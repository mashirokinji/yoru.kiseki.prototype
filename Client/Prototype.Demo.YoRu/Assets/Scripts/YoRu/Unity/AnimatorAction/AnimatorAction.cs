﻿using UnityEngine;

namespace YoRu.Unity.Mono
{
	public abstract class AnimatorAction : ScriptableObject
	{
		public const string CREATE_ASSET_MENU_PATH = "Animator action/";

		public abstract void Do(Animator aAnimator);
	}
}