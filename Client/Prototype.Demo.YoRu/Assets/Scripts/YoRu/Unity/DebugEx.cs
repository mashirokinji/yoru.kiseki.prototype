﻿using UnityEngine;
using UnityEngine.AI;

namespace YoRu.Unity
{
	public static class DebugEx
	{
		public static void DrawPath(NavMeshPath aPath, Color aColor)
		{
			Vector3? oc = null;
			foreach(var tc in aPath.corners)
			{
				if (oc != null) Debug.DrawLine(oc.Value, tc, aColor);
				oc = tc;
			}
		}

		public static void DrawDisc(Vector3 aPosition, Vector3 aDirection, float aRadius, Color aColor)
		{
			Vector3 v = (Vector3.one - aDirection.normalized).normalized * aRadius;
			float angleDelta = YrMath.FULL_ANGLE_DEGREE / CIRCLE_EDGE_COUNT;
			for (int i = 0; i < CIRCLE_EDGE_COUNT; i++)
			{
				var v1 = aPosition + Quaternion.Euler(aDirection * i * angleDelta) * v;
				var v2 = aPosition + Quaternion.Euler(aDirection * (i + 1) * angleDelta) * v;
				Debug.DrawLine(v1, v2, aColor);
			}
		}

		private const int CIRCLE_EDGE_COUNT = 10;
	}
}
