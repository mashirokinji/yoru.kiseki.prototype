﻿using UnityEngine;

namespace YoRu.Unity.Mono
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Animator))]
	public class AnimatorActionPlayer : YrBehaviour
	{
		private void Awake()
		{
			m_Animator = GetComponent<Animator>();
		}

		public void Play(AnimatorAction aAnimatorAction)
		{
			aAnimatorAction.Do(m_Animator);
		}

		private Animator m_Animator;
	}
}