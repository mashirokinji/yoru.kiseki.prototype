﻿using System;
using UnityEngine;

namespace YoRu.Unity.Mono
{
	public sealed class AxesButton : YrBehaviour
	{
		public event Action onClick 
		{
			add { m_InnerAxesButton.onClick += value; }
			remove { m_InnerAxesButton.onClick -= value; }
		}

		private sealed class InnerAxesButton : BaseAxesButton
		{
			public InnerAxesButton(AxesButton aOwner)
			{
				m_Owner = aOwner;
			}

			public override string axesName => m_Owner.m_AxesName;

			private readonly AxesButton m_Owner;
		}

		private void Awake()
		{
			m_InnerAxesButton = new InnerAxesButton(this);
		}

		private void Update()
		{
			m_InnerAxesButton.Update();
		}

		private InnerAxesButton m_InnerAxesButton;

#pragma warning disable 0649
		[SerializeField] private string m_AxesName;
#pragma warning restore 0649
	}
}