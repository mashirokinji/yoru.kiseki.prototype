﻿using UnityEngine;

namespace YoRu.Unity
{
	public static class Vector2Ex
	{
		public static Vector2 Rotate(this Vector2 aValue, float aRadian)
		{
			var x = aValue.x;
			var y = aValue.y;

			return new Vector2(
					x * Mathf.Cos(aRadian) - y * Mathf.Sin(aRadian),
					x * Mathf.Sin(aRadian) + y * Mathf.Cos(aRadian));
		}

		public static Vector2 To0Y(this Vector2 aValue)
		{
			return new Vector2(0, aValue.y);
		}

		public static Vector2 ToX0(this Vector2 aValue)
		{
			return new Vector2(aValue.x, 0);
		}

		public static Vector3 ToXY0(this Vector2 aValue)
		{
			return new Vector3(aValue.x, aValue.y, 0);
		}

		public static Vector3 ToX0Y(this Vector2 aValue)
		{
			return new Vector3(aValue.x, 0, aValue.y);
		}

		public static Vector3 ToYX0(this Vector2 aValue)
		{
			return new Vector3(aValue.y, aValue.x, 0);
		}

		public static Vector3 ToY0X(this Vector2 aValue)
		{
			return new Vector3(aValue.y, 0, aValue.x);
		}

		public static Vector3 To0XY(this Vector2 aValue)
		{
			return new Vector3(0, aValue.x, aValue.y);
		}

		public static Vector3 To0YX(this Vector2 aValue)
		{
			return new Vector3(0, aValue.y, aValue.x);
		}
	}
}
