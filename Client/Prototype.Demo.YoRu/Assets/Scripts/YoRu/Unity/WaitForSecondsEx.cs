﻿using UnityEngine;
using System.Collections.Generic;

namespace YoRu.Unity
{
	public static class WaitForSecondsEx
	{
		public static WaitForSeconds Get(float aSeconds)
		{
			if (!WAIT_FOR_SECONDS_CACHES.TryGetValue(aSeconds, out WaitForSeconds waitForSeconds))
			{
				WAIT_FOR_SECONDS_CACHES.Add(aSeconds, waitForSeconds = new WaitForSeconds(aSeconds));
			}
			return waitForSeconds;
		}

		private static readonly Dictionary<float, WaitForSeconds> WAIT_FOR_SECONDS_CACHES =
			new Dictionary<float, WaitForSeconds>();
	}
}
