﻿using UnityEngine;

namespace YoRu.Unity
{
	public static class Vector3Ex
	{
		public static Vector3 To0YZ(this Vector3 value)
		{
			return new Vector3(0, value.y, value.z);
		}

		public static Vector3 ToX0Z(this Vector3 value)
		{
			return new Vector3(value.x, 0, value.z);
		}

		public static Vector3 ToXY0(this Vector3 value)
		{
			return new Vector3(value.x, value.y, 0);
		}

		public static Vector3 ToX00(this Vector3 value)
		{
			return new Vector3(value.x, 0, 0);
		}

		public static Vector3 To0Y0(this Vector3 value)
		{
			return new Vector3(0, value.y, 0);
		}

		public static Vector3 To00Z(this Vector3 value)
		{
			return new Vector3(0, 0, value.z);
		}

		public static Vector2 ToXY(this Vector3 value)
		{
			return new Vector2(value.x, value.y);
		}

		public static Vector2 ToXZ(this Vector3 value)
		{
			return new Vector2(value.x, value.z);
		}

		public static Vector2 ToYX(this Vector3 value)
		{
			return new Vector2(value.y, value.x);
		}

		public static Vector2 ToYZ(this Vector3 value)
		{
			return new Vector2(value.y, value.z);
		}

		public static Vector2 ToZX(this Vector3 value)
		{
			return new Vector2(value.z, value.x);
		}

		public static Vector2 ToZY(this Vector3 value)
		{
			return new Vector2(value.z, value.y);
		}
	}
}
