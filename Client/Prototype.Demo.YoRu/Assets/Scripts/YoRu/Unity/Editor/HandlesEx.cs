﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using System;
#pragma warning disable 0649

namespace YoRu.Unity.Editor
{
	public class HandlesEx
	{
		public static void DrawWithColor(Color color, Action action)
		{
			var tempColor = Handles.color;
			Handles.color = color;
			action?.Invoke();
			Handles.color = tempColor;
		}
	}
}