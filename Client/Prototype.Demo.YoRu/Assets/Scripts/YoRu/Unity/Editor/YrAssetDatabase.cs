﻿using UnityEditor;
using System;

namespace YoRu.Unity.Editor
{
	public static class YrAssetDatabase
	{
		public static string[] GetPathsByFilter(string filter)
		{
			return Array.ConvertAll(AssetDatabase.FindAssets(filter), AssetDatabase.GUIDToAssetPath);
		}

		public static T[] LoadAssetsByFilter<T>(string filter) where T : UnityEngine.Object
		{
			var paths = GetPathsByFilter(filter);
			T[] objects = new T[paths.Length];
			for (int i = 0; i < paths.Length; i++)
			{
				objects[i] = AssetDatabase.LoadAssetAtPath<T>(paths[i]);
			}
			return objects;
		}
	}
}