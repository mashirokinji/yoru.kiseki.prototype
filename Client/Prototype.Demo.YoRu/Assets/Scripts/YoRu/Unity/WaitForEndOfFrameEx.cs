﻿using UnityEngine;

namespace YoRu.Unity
{
	public static class WaitForEndOfFrameEx
	{
		public static readonly WaitForEndOfFrame DEFAULT = new WaitForEndOfFrame();
	}
}
