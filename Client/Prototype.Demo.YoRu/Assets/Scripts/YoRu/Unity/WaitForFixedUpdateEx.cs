﻿using UnityEngine;

namespace YoRu.Unity
{
	public static class WaitForFixedUpdateEx
	{
		public static readonly WaitForFixedUpdate DEFAULT = new WaitForFixedUpdate();
	}
}
