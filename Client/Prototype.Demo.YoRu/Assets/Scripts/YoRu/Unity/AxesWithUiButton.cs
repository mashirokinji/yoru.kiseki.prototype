﻿using UnityEngine;
using System;
using YoRu.Unity.Mono;
using UnityEngine.UI;

namespace YoRu.Unity
{
	[RequireComponent(typeof(Button))]
	[RequireComponent(typeof(AxesButton))]
	public sealed class AxesWithUiButton : YrBehaviour
	{
		private void Awake()
		{
			m_Button = gameObject.GetComponent<Button>();
			m_AxesButtonListener = GetComponent<AxesButton>();
		}

		private void OnEnable()
		{
			m_Button.onClick.AddListener(OnClick);
			m_AxesButtonListener.onClick += OnClick;
		}

		private void OnDisable()
		{
			m_AxesButtonListener.onClick -= OnClick;
			m_Button.onClick.RemoveListener(OnClick);
		}

		private void OnClick()
		{
			onClick?.Invoke();
		}

		public event Action onClick;

		private Button m_Button;
		private AxesButton m_AxesButtonListener;
	}
}