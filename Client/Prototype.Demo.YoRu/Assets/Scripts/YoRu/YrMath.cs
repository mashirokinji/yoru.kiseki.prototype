﻿using UnityEngine;
namespace YoRu
{
	public static class YrMath
	{
		public const float FULL_ANGLE_DEGREE = 360;
		public const float THREE_RIGHT_ANGLE_DEGREE = 270;
		public const float STRAIGHT_ANGLE_DEGREE = 180;
		public const float RIGHT_ANGLE_DEGREE = 90;
		public const float ZERO_ANGLE_DEGREE = 0;

		public const float FULL_ANGLE_RADIAN = Mathf.PI * 2;
		public const float THREE_RIGHT_ANGLE_RADIAN = Mathf.PI * 1.5f;
		public const float STRAIGHT_ANGLE_RADIAN = Mathf.PI;
		public const float RIGHT_ANGLE_RADIAN = Mathf.PI * 0.5f;
		public const float ZERO_ANGLE_RADIAN = 0;

		//public static float Sin(float degree)
		//{
		//	return Mathf.Sin(degree * Mathf.Deg2Rad);
		//}

		//public static float Cos(float degree)
		//{
		//	return Mathf.Cos(degree * Mathf.Deg2Rad);
		//}

		//public static float Tan(float degree)
		//{
		//	return Mathf.Tan(degree * Mathf.Deg2Rad);
		//}
	}
}
