﻿using System;
using System.Collections.Generic;

namespace YoRu
{
	public static class IntEx
	{
		public static int Square(this int value)
		{
			return value * value;
		}
	}

	public class YrException : Exception 
	{
		public YrException() { }
		public YrException(string msg) : base(msg) { }
	}

	public class FiniteStateMachine
	{
		public abstract class Item : YrObject
		{
			public Item(object aToken)
			{
				if (TOKEN != aToken) throw new YrException("Please create " + GetType().Name + "By FiniteStateMachine.CreateXXXXX()");
			}
		}

		private static readonly object TOKEN = new object();
	}

	public class FiniteStateMachineObject : YrObject
	{
		public FiniteStateMachineObject(FiniteStateMachine aFiniteStateMachine)
		{
			m_FiniteStateMachine = aFiniteStateMachine;
		}
		protected FiniteStateMachine finiteStateMachine { get { return m_FiniteStateMachine; } }

		private readonly FiniteStateMachine m_FiniteStateMachine;
	}

	public class FiniteStateMachineState : FiniteStateMachineObject
	{
		public FiniteStateMachineState(FiniteStateMachine aFiniteStateMachine) : base(aFiniteStateMachine)
		{
		}

		private readonly List<BaseFiniteStateMachineBehaviour> m_Behaviours = new List<BaseFiniteStateMachineBehaviour>();
		private readonly List<FiniteStateMachineTransform> m_Transforms = new List<FiniteStateMachineTransform>();
	}

	public abstract class BaseFiniteStateMachineCondition : FiniteStateMachineObject
	{
		public BaseFiniteStateMachineCondition(FiniteStateMachine aFiniteStateMachine) : base(aFiniteStateMachine)
		{
		}

		public abstract bool IsMeet();
	}

	public abstract class BaseFiniteStateMachineBehaviour : FiniteStateMachineObject
	{
		public BaseFiniteStateMachineBehaviour(FiniteStateMachine aFiniteStateMachine) : base(aFiniteStateMachine)
		{
		}

		public abstract void Enter();
		public abstract void Exit();
		public abstract void Update();
	}

	public class FiniteStateMachineTransform : FiniteStateMachineObject
	{
		public FiniteStateMachineTransform(FiniteStateMachine aFiniteStateMachine) : base(aFiniteStateMachine)
		{
		}

		private FiniteStateMachineState m_Target;
		private BaseFiniteStateMachineCondition[] m_Conditons;
	}
}
