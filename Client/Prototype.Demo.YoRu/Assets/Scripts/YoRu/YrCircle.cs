﻿using UnityEngine;

namespace YoRu
{
	public struct YrCircle
	{
		public YrCircle(float aX, float aY, float aRadius)
		{
			m_Center = new Vector2(aX, aY);
			radius = aRadius;
		}

		public YrCircle(Vector2 aCenter, float aRadius)
		{
			m_Center = aCenter;
			radius = aRadius;
		}

		public bool Contain(YrCircle circle)
		{
			return (center - circle.center).sqrMagnitude <= (radius + circle.radius).Square();
		}

		public float x { get { return m_Center.x; } set { m_Center.x = value; } }
		public float y { get { return m_Center.y; } set { m_Center.y = value; } }

		public Vector2 center { get => m_Center; set => m_Center = value; }
		public float radius { get; set; }

		private Vector2 m_Center;
	}
}