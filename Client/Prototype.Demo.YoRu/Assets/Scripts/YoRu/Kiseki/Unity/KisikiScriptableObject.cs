﻿using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	public abstract class KisikiScriptableObject : ScriptableObject
	{
		public const string CREATE_ASSET_MENU_PATH = "Kisiki/";
	}
}
