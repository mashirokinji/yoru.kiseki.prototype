﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace YoRu.Kiseki.Unity
{
	[Serializable]
	public class MainAnalogStickListener : YrObject
	{
		public delegate void FOnInput(Vector2 aDirection);
		public delegate void FOnChange(Vector2 aDirection);

		public void Update()
		{
			Vector2 prevDirection = curDirection;
			curDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

			onInput?.Invoke(curDirection);
			m_OnInput?.Invoke(curDirection);

			if (prevDirection != curDirection)
			{
				onChange?.Invoke(curDirection);
				m_OnChange?.Invoke(curDirection);
			}
		}

		public event FOnInput onInput;
		public event FOnChange onChange;

		public Vector2 curDirection { get; set; }

		[Serializable] private class OnChangeEvent : UnityEvent<Vector2> { }

#pragma warning disable 0649
		[SerializeField] private OnChangeEvent m_OnInput;
		[SerializeField] private OnChangeEvent m_OnChange;
#pragma warning restore 0649
	}
}