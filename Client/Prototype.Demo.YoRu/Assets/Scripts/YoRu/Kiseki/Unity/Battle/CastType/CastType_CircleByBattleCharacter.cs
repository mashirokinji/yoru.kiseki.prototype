using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[System.Serializable]
	public sealed class CastType_CircleByBattleCharacter : CastType
	{
		public override void DrawRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster)
		{
			if(aPen == null) return;
			if(aCaster == null) return;
			aPen.DrawCastCircle(new YrCircle(aCaster.position, m_Rng));
		}

		public override void DrawAttackRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster, ICastTypeUnitCom aTarget, Vector2 aPosition)
		{
			if(aPen == null) return;
			if(aTarget == null) return;
			aPen.DrawAttackCircle(new YrCircle(aTarget.position, m_Rng));
		}

		public override void Sample<T>(Vector2 aPositionParam, T aUnitParam,
			T aCaster, ReadOnlyList<T> aBackupUnit, ref BattleCastResult<T> aResult)
		{
			aResult.Normalized();
			if (!(aResult.isVaild = SampleVaildCastTarget(aPositionParam, aUnitParam, aCaster,
				out Vector2? newPosition))) return;
			var buffer = aResult.effectBattleCharacterBuffer;
			foreach (var unit in aBackupUnit)
			{
				if (unit == null) continue;
				if (!m_BattleEffectTargetType.IsMeet(aCaster, unit)) continue;
				if (!new YrCircle(aUnitParam.position, m_Radius).
					Contain(new YrCircle(unit.position, unit.radius))) continue;
				buffer.Add(unit);
			}
		}

		public override bool SampleVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster, out Vector2? aNewPosition)
		{
			aNewPosition = null;
			if (!m_BattleCastTargetType.IsMeet(aCaster, aUnitParam)) return false;
			if (!aCaster.SampleAttack(aUnitParam.position, m_Radius, m_CanMove, m_Rng, out aNewPosition)) return false;
			return true;
		}

		public override CastTypeParamType paramType => CastTypeParamType.CHARACTER;

#pragma warning disable 0649
		[SerializeField] private bool m_CanMove;
		[SerializeField] private uint m_Rng;
		[SerializeField] private BattleTargetType m_BattleCastTargetType;
		[SerializeField] private BattleTargetType m_BattleEffectTargetType;
		[SerializeField] private uint m_Radius;
#pragma warning restore 0649
	}
}