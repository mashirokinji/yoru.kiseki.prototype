﻿using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = CREATE_ASSET_MENU_PATH + "Line by position")]
	public sealed class CraftCastTypeSetting_LineByPosition : CriftCastTypeSetting<CastType_LineByPosition> { }
}