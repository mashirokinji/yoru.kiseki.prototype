namespace YoRu.Kiseki.Unity
{
	public static class BattleEffectTargetTypeEx
	{
		public static bool IsMeet(this BattleTargetType aBattleEffectTargetType, IBattleTeamMember aCaster, IBattleTeamMember aTarget)
		{
			if((aBattleEffectTargetType & BattleTargetType.ENEMY) > 0)
			{
				return aCaster.teamId != aTarget.teamId;
			}

			if((aBattleEffectTargetType & BattleTargetType.FRIEND) > 0)
			{
				return aCaster.teamId != aTarget.teamId && aCaster != aTarget;
			}

			if((aBattleEffectTargetType & BattleTargetType.SLEF) > 0)
			{
				return aCaster != aTarget;
			}
			return false;
		}
	}
}

