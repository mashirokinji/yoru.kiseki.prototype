using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	public abstract class CraftCastTypeSetting : CastTypeSetting
	{
		public new const string CREATE_ASSET_MENU_PATH = CastTypeSetting.CREATE_ASSET_MENU_PATH + "Crift/";
	}

	public abstract class CriftCastTypeSetting<TCastType> : CraftCastTypeSetting where TCastType : CastType, new()
	{
		protected sealed override CastType castType { get { return m_CastType;} }

#pragma warning disable 0649
		[SerializeField] private TCastType m_CastType;
#pragma warning restore 0649
	}
}