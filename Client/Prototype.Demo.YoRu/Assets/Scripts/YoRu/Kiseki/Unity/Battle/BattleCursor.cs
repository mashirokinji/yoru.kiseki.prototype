﻿using UnityEngine;
using UnityEngine.AI;
using YoRu.Unity;
using UnityEngine.Events;
using System;
using YoRu.Kiseki.Unity;
using Button1Listener = YoRu.Kiseki.Unity.Button1Listener;
using Button2Listener = YoRu.Kiseki.Unity.Button2Listener;

namespace YoRu.Kiseki
{
	public class VisibleObject : KisekiObject
	{
		public bool isShow
		{
			get { return m_IsShow; }
			set
			{
				if (m_IsShow = value) return;
				m_IsShow = value;
				if (m_IsShow)
				{
					onShow?.Invoke();
				}
				else
				{
					onHide?.Invoke();
				}
			}
		}

		public event Action onShow;
		public event Action onHide;
		private bool m_IsShow;
	}
}

namespace YoRu.Kiseki.Unity
{
	public sealed class BattleCursor : KisekiBehaviour
	{
		public event Action onShow { add { m_VisibleObject.onShow += value; } remove { m_VisibleObject.onShow += value; } }
		public event Action onHide { add { m_VisibleObject.onHide += value; } remove { m_VisibleObject.onHide += value; } }

		public bool isShow { get { return m_VisibleObject.isShow; } set { m_VisibleObject.isShow = value; } }

		public Vector3 position
		{
			get { return m_Transform.position; }
			set
			{
				NavMesh.SamplePosition(value, out NavMeshHit hit, 1, NavMesh.AllAreas);
				m_Transform.position = hit.position;
			}
		}

		public float range { get { return m_Range; } set { m_Range = value; } }

		private void Awake()
		{
			m_Transform = transform;
			m_CameraTransform = m_Camera.transform;
			m_VisibleObject = new VisibleObject();
		}

		private void OnEnable()
		{
			m_VisibleObject.onShow += VisibleObject_OnShow;
			m_VisibleObject.onHide += VisibleObject_OnHide;
			m_MainAnalogStickListener.onInput += MainAnalogStickListener_OnInput;
		}

		private void OnDisable()
		{
			m_MainAnalogStickListener.onInput -= MainAnalogStickListener_OnInput;
			m_VisibleObject.onHide -= VisibleObject_OnHide;
			m_VisibleObject.onShow -= VisibleObject_OnShow;
		}

		private void Update()
		{
			m_MainAnalogStickListener.Update();
			m_ConfirmButtonListener.Update();
			m_CancelButtonListener.Update();
		}

		private void MainAnalogStickListener_OnInput(Vector2 direction)
		{
			Vector3 offset = m_CameraTransform.forward.ToX0Z().normalized * direction.y;
			offset += m_CameraTransform.right.ToX0Z().normalized * direction.x;
			offset *= Time.deltaTime * m_Speed;
			position = m_Transform.position + offset;
		}

		private void VisibleObject_OnShow()
		{
			m_OnShow?.Invoke();
		}

		private void VisibleObject_OnHide()
		{
			m_OnHide?.Invoke();
		}

		private VisibleObject m_VisibleObject;
		private Transform m_Transform;
		private Transform m_CameraTransform;

#pragma warning disable 0649
		[SerializeField] private Camera m_Camera;
		[SerializeField] private float m_Range;
		[SerializeField] private float m_Speed = 1;
		[SerializeField] private MainAnalogStickListener m_MainAnalogStickListener;
		[SerializeField] private Unity.Button1Listener m_ConfirmButtonListener;
		[SerializeField] private Unity.Button2Listener m_CancelButtonListener;
		[SerializeField] private UnityEvent m_OnShow;
		[SerializeField] private UnityEvent m_OnHide;
#pragma warning restore 0649
	}
}