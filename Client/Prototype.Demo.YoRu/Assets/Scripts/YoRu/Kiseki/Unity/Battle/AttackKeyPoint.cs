﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

namespace YoRu.Kiseki.Unity
{
	[Serializable]
	public sealed class AttackKeyPoint : YrObject
	{
		private AttackKeyPoint() { }

		//public sealed class Executer : IEnumerator, IYrPoolObject<Executer.InitParam>
		//{
		//	public struct InitParam
		//	{
		//		public AttackKeyPoint owner { get; set; }
		//		public IAttackKeyPointActionUnitCom caster { get; set; }
		//		public IAttackKeyPointActionUnitCom target { get; set; }
		//	}

		//	private void Reset()
		//	{
		//		m_HasMovedNext = false;
		//		m_AttackKeyPointActionEnumerator = new ActionEnumerator();
		//		if(m_Executer != null && m_ExecutingAction != null)
		//		{
		//			m_ExecutingAction.RecycleExecuter(m_Executer);
		//			m_Executer = null;
		//			m_ExecutingAction = null;
		//		}
		//	}

		//	void IYrPoolObject<InitParam>.Init(InitParam aParam)
		//	{
		//		m_Owner = aParam.owner;
		//		m_Caster = aParam.caster;
		//		m_Target = aParam.target;
		//	}

		//	void IYrPoolObject<InitParam>.Release()
		//	{
		//		m_Target = null;
		//		m_Caster = null;
		//		m_Owner = null;
		//	}

		//	void IYrPoolObject<InitParam>.Reset() => Reset();

		//	object IEnumerator.Current => null;

		//	bool IEnumerator.MoveNext()
		//	{
		//		if (m_Owner == null) return false;

		//		m_Owner.m_OnExecute?.Invoke();

		//		if (m_Executer != null && m_ExecutingAction != null)
		//		{
		//			if (m_Executer.MoveNext()) return true;
		//			m_ExecutingAction.RecycleExecuter(m_Executer);
		//			m_Executer = null;
		//			m_ExecutingAction = null;
		//		}

		//		if (!m_HasMovedNext)
		//		{
		//			m_AttackKeyPointActionEnumerator = m_Owner.GetActionEnumerator();
		//			m_HasMovedNext = true;
		//		}
		//		if (m_AttackKeyPointActionEnumerator.MoveNext())
		//		{
		//			m_ExecutingAction = m_AttackKeyPointActionEnumerator.Current;
		//			m_Executer = m_ExecutingAction.GetExecuter(new AttackKeyPointAction.ExecuterParam
		//			{
		//				caster = m_Caster,
		//				target = m_Target,
		//			});
		//			return true;
		//		}
		//		return false;
		//	}

		//	void IEnumerator.Reset() => Reset();

		//	private AttackKeyPoint m_Owner;
		//	private ActionEnumerator m_AttackKeyPointActionEnumerator;
		//	private AttackKeyPointAction m_ExecutingAction;
		//	private AttackKeyPointAction.Executer m_Executer;
		//	private IAttackKeyPointActionUnitCom m_Caster;
		//	private IAttackKeyPointActionUnitCom m_Target;
		//	private bool m_HasMovedNext;
		//}

		//public struct ExecuterParam
		//{
		//	public IAttackKeyPointActionUnitCom caster { get; set; }
		//	public IAttackKeyPointActionUnitCom target { get; set; }
		//}

		//public Executer GetExectuer(ExecuterParam aExecuterParam) =>
		//	YrPoolCenter<Executer, Executer.InitParam>.Get(
		//		new Executer.InitParam()
		//		{
		//			owner = this,
		//			caster = aExecuterParam.caster,
		//			target = aExecuterParam.target,
		//		});

		//public void Recycle(Executer aExecuter) => YrPoolCenter<Executer, Executer.InitParam>.Recycle(aExecuter);
		public IEnumerator CastSync(BattleCore aBattleCore, BattleCharacter aCaster, BattleCharacter aTargetCharacter, Vector2 aTargetPosition)
		{
			using (var e = GetActionEnumerator())
			{
				while(e.MoveNext())
				{
					var action = e.Current;
					if (action == null) continue;
					var playSyncEnumerator = action.PlaySync(aBattleCore, aCaster, aTargetCharacter);
					if(playSyncEnumerator != null)
					{
						while (playSyncEnumerator.MoveNext())
						{
							yield return playSyncEnumerator.Current;
						}
					}
				}
			}
		}
		private struct ActionEnumerator : IEnumerator<AttackKeyPointAction>
		{
			internal ActionEnumerator(AttackKeyPoint owner)
			{
				if(owner.m_AttackKeyPointActions != null) m_Enumerator = owner.m_AttackKeyPointActions.GetEnumerator();
			}

			public void Dispose() => m_Enumerator.Dispose();

			public bool MoveNext() => m_Enumerator.MoveNext();

			public void Reset() => ((IEnumerator)m_Enumerator).Reset();

			public AttackKeyPointAction Current => m_Enumerator.Current;

			private List<AttackKeyPointAction>.Enumerator m_Enumerator;

			object IEnumerator.Current => Current; 
		}

		private ActionEnumerator GetActionEnumerator() => new ActionEnumerator(this);

		public float offsetTime { get { return m_OffsetTime; } }

		// private
#pragma warning disable 0649
		[SerializeField] private float m_OffsetTime;
		[SerializeField] private List<AttackKeyPointAction> m_AttackKeyPointActions;
#pragma warning restore 0649
	}
}