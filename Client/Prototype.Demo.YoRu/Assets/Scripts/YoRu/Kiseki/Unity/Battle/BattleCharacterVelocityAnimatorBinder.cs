﻿using UnityEngine;
using YoRu.Unity.Mono;

namespace YoRu.Kiseki.Unity
{
	[RequireComponent(typeof(BattleCharacterSetting))]
	public class BattleCharacterVelocityAnimatorBinder : YrBehaviour
	{
		private void Awake()
		{
			m_BattleCharacter = GetComponent<BattleCharacterSetting>();
		}

		private void Update()
		{
			if (m_BattleCharacter is null) throw new MissingComponentException();
			if (m_Animator is null) throw new MissingComponentException();
			m_Animator.SetFloat(m_ParameterName, m_BattleCharacter.velocity.sqrMagnitude);
		}

		private BattleCharacterSetting m_BattleCharacter;

#pragma warning disable 0649
		[SerializeField] private Animator m_Animator;
		[SerializeField] private string m_ParameterName;
#pragma warning restore 0649
	}
}