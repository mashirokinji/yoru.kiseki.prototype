using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	public interface ICastTypeUnitCom : IBattleTeamMember
	{
		bool SampleAttack(Vector2 aPosition, float aRadius, bool aCanMove, uint aRng, out Vector2? aNewPosition);
		bool IsVaildMovePosition(Vector2 aPosition);
		Vector2 position { get; }
		float radius { get; }
		uint mov { get; }
	}
}