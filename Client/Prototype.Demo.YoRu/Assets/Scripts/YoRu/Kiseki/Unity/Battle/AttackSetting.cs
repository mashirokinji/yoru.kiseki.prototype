﻿using System;
using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[Serializable]
	public sealed class AttackSetting : YrObject
	{
		public AttackKeyPoint GetAttackKeyPoint(int index)
		{
			return m_AttackKeyPoints[index];
		}

		public int attackKeyPointLength { get { return m_AttackKeyPoints.Length; } }
		public float baseTime { get { return m_BaseTime; } }

		// private
#pragma warning disable 0649
		[SerializeField] private float m_BaseTime;
		[SerializeField] private AttackKeyPoint[] m_AttackKeyPoints;
#pragma warning restore 0649
	}
}