﻿using System.IO;
using System.Collections;

namespace YoRu.Kiseki.Unity
{
	public abstract class AttackKeyPointAction : KisikiScriptableObject
	{
		//public abstract class Executer : KisekiObject, IEnumerator, IYrPoolObject<Executer.InitParam>
		//{
		//	public struct InitParam
		//	{
		//		public AttackKeyPointAction owner { get; set; }
		//		public IAttackKeyPointActionUnitCom caster { get; set; }
		//		public IAttackKeyPointActionUnitCom target { get; set; }
		//	}

		//	public object Current => null;

		//	public abstract bool MoveNext();

		//	public abstract void Reset();
		//	protected abstract void Init(InitParam aParam);
		//	protected abstract void Release();

		//	void IYrPoolObject<InitParam>.Init(InitParam aParam) => Init(aParam);

		//	void IYrPoolObject<InitParam>.Release() => Release();
		//}

		//public struct ExecuterParam
		//{
		//	public IAttackKeyPointActionUnitCom caster { get; set; }
		//	public IAttackKeyPointActionUnitCom target { get; set; }
		//}

		public abstract IEnumerator PlaySync(BattleCore aBattleCore, BattleCharacter aCaster, BattleCharacter aTarget);

		public new const string CREATE_ASSET_MENU_PATH = KisikiScriptableObject.CREATE_ASSET_MENU_PATH + "Attack key point action/";

		public static bool debug { get; set; } = true;

		//public abstract Executer GetExecuter(ExecuterParam aInitParam);

		//public abstract void RecycleExecuter(Executer aExecuter);
	}

}
