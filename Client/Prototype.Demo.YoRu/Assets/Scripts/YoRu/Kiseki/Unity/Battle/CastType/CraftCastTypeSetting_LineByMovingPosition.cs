﻿using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = CREATE_ASSET_MENU_PATH + "Line by moving position")]
	public sealed class CraftCastTypeSetting_LineByMovingPosition : CriftCastTypeSetting<CastType_LineByMovingPosition> { }
}