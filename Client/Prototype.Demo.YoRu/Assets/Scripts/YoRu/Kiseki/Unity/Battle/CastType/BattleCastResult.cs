﻿using System.Collections.Generic;
using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	public struct BattleCastResult<T> where T : ICastTypeUnitCom
	{
		// If buffer is null put a new list into buffer. Otherwise clear the buffer list. And reset else member at least.
		public void Normalized()
		{
			isVaild = false;
			newPosition = null;
			if (effectBattleCharacterBuffer == null)
			{
				effectBattleCharacterBuffer = new List<T>();
			}
			else
			{
				effectBattleCharacterBuffer?.Clear();
			}
		}

		public bool isVaild { get; set; }
		public Vector2? newPosition { get; set; }
		public List<T> effectBattleCharacterBuffer { get; set; }
	}

}