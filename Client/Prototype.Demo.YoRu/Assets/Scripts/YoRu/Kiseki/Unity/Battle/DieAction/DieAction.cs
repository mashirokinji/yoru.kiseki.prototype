﻿namespace YoRu.Kiseki.Unity
{
	public abstract class DieAction : KisikiScriptableObject
	{
		public new const string CREATE_ASSET_MENU_PATH = KisikiScriptableObject.CREATE_ASSET_MENU_PATH + "Die action/";

		public static bool debug { get; set; } = true;

		public abstract void Do();
	}
}
