﻿using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = CREATE_ASSET_MENU_PATH + "Circle by attack position")]
	public sealed class CraftCastTypeSetting_CircleByAttackPosition : CriftCastTypeSetting<CastType_CircleByAttackPosition> { }
}