using System;
using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[Serializable]
	public abstract class CastType
	{
		public abstract void DrawRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster);

		public abstract void DrawAttackRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster, ICastTypeUnitCom aTarget, Vector2 aPosition);

		public abstract void Sample<T>(Vector2 aPositionParam, T aBattleCharacterParam,
			T aCaster, ReadOnlyList<T> aBackupUnit, ref BattleCastResult<T> aResult) where T : ICastTypeUnitCom;

		public abstract bool SampleVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster, out Vector2? aNewPosition);

		public bool IsVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster) => SampleVaildCastTarget(aPositionParam, aUnitParam, aCaster, out Vector2? newPosition);

		public abstract CastTypeParamType paramType { get; }
	}
}