﻿namespace YoRu.Kiseki.Unity
{
	public enum CastValidTag
	{
		INVALID,
		VALID_AFTER_MOVING,
		VALID,
	}
}