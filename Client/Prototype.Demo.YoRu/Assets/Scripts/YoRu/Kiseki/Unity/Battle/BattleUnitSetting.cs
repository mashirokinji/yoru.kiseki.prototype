﻿using UnityEngine;
namespace YoRu.Kiseki.Unity
{
	public class BattleUnitSetting : KisekiBehaviour
	{
	}

	public interface IBattleTeamMember
	{
		byte teamId { get; }
	}

	public interface IBattleCharacterInfo : IBattleTeamMember
	{
		bool IsInAttackRng(IBattleCharacterInfo aIndex);
		bool IsValidMovePosition(Vector2 aPosition);

		string name { get; }
		Transform transform { get; }
		Vector2 position { get; }
		float radius { get; }
		uint maxHp { get; }
		uint hp { get; }
		uint str { get; }
		uint def { get; }
		uint mov { get; }
		uint rng { get; }
		int delay { get; }
	}

}