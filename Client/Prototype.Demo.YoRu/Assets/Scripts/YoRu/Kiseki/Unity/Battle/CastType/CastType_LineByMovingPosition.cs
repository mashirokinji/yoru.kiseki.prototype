using System;
using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[System.Serializable]
	public sealed class CastType_LineByMovingPosition : CastType
	{
		public override void DrawRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster)
		{
			throw new NotImplementedException();
		}

		public override void DrawAttackRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster, ICastTypeUnitCom aTarget, Vector2 aPosition)
		{
			throw new NotImplementedException();
		}

		public override void Sample<T>(Vector2 aPositionParam, T aUnitParam,
			T aCaster, ReadOnlyList<T> aBackupUnit, ref BattleCastResult<T> aResult)
		{
			aResult.Normalized();
			if (!(aResult.isVaild = SampleVaildCastTarget(aPositionParam, aUnitParam, aCaster,
				out Vector2? aNewPosition))) return;
		}

		public override bool SampleVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster, out Vector2? aNewPosition)
		{
			throw new NotImplementedException();
		}

		public override CastTypeParamType paramType => CastTypeParamType.POSITION;

#pragma warning disable 0649
		[SerializeField] private uint m_Size;
#pragma warning restore 0649
	}

}