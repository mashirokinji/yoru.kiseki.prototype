using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace YoRu.Kiseki.Unity
{
	[Serializable]
	public sealed class CraftSetting /* IExecutorManager */
	{
		private CraftSetting() { }

		//public struct ExecutorParam
		//{
		//	public IAttackKeyPointActionUnitCom caster{ get; set; }
		//	public IAttackKeyPointActionUnitCom target{ get; set; }
		//}

		//public sealed class Executor : IYrPoolObject<Executor.InitParam>, IExecutor
		//{
		//	public struct InitParam
		//	{
		//		public CraftSetting craftSetting { get; set; }
		//		public IAttackKeyPointActionUnitCom caster { get; set; }
		//		public IAttackKeyPointActionUnitCom target { get; set; }
		//	}

		//	private void Reset()
		//	{
		//		m_HasMovedNext = false;
		//		m_AttackKeyPointEnumerator = new AttackKeyPointEnumerator();
		//		if(m_ExecutingAttackKeyPoint != null && m_Executer != null)
		//		{
		//			m_ExecutingAttackKeyPoint.Recycle(m_Executer);
		//			m_Executer = null;
		//			m_ExecutingAttackKeyPoint = null;
		//		}
		//	}

		//	void IYrPoolObject<InitParam>.Init(InitParam aParam)
		//	{
		//		m_CraftSetting = aParam.craftSetting;
		//		m_Caster = aParam.caster;
		//		m_Target = aParam.target;
		//	}

		//	void IYrPoolObject<InitParam>.Release()
		//	{
		//		m_Target = null;
		//		m_Caster = null;
		//		m_CraftSetting = null;
		//	}

		//	void IYrPoolObject<InitParam>.Reset() => Reset();

		//	bool IEnumerator.MoveNext()
		//	{
		//		if (m_CraftSetting == null) return false;

		//		if (m_ExecutingAttackKeyPoint != null && m_Executer != null)
		//		{
		//			if (((IEnumerator)m_Executer).MoveNext()) return true;
		//			m_ExecutingAttackKeyPoint.Recycle(m_Executer);
		//			m_Executer = null;
		//			m_ExecutingAttackKeyPoint = null;
		//		}

		//		if (!m_HasMovedNext)
		//		{
		//			m_AttackKeyPointEnumerator = m_CraftSetting.GetAttackKeyPointEnumerator();
		//			m_HasMovedNext = true;
		//		}
		//		if (m_AttackKeyPointEnumerator.MoveNext())
		//		{
		//			m_ExecutingAttackKeyPoint = m_AttackKeyPointEnumerator.Current;
		//			m_Executer = m_ExecutingAttackKeyPoint.GetExectuer(new AttackKeyPoint.ExecuterParam
		//			{
		//				caster = m_Caster,
		//				target = m_Target,
		//			});
		//			return true;
		//		}
		//		return false;
		//	}

		//	void IEnumerator.Reset() => Reset();

		//	object IEnumerator.Current => null;

		//	private CraftSetting m_CraftSetting;
		//	private IAttackKeyPointActionUnitCom m_Caster;
		//	private IAttackKeyPointActionUnitCom m_Target;

		//	private AttackKeyPointEnumerator m_AttackKeyPointEnumerator;
		//	private AttackKeyPoint m_ExecutingAttackKeyPoint;
		//	private AttackKeyPoint.Executer m_Executer;
		//	private bool m_HasMovedNext;
		//}

		//public IExecutor GetExecutor(ExecutorParam aParam)
		//{
		//	return YrPoolCenter<Executor, Executor.InitParam>.Get(new Executor.InitParam
		//	{
		//		craftSetting = this,
		//		caster = aParam.caster,
		//		target = aParam.target,
		//	});
		//}

		//public void RecycleExecutor(IExecutor aExecutor)
		//{
		//	YrPoolCenter<Executor, Executor.InitParam>.Recycle((Executor)aExecutor);
		//}

		public IEnumerator CastSync(BattleCore aBattleCore, BattleCharacter aCaster, BattleCharacter aTargetCharacter, Vector2 aTargetPosition)
		{
			if(!SampleCast(aCaster, aTargetCharacter, aTargetPosition, out Vector2? aNewPosition)) yield break;
			if (aCaster.cp < costCp) yield break;

			aCaster.delay += delayTime;
			aCaster.cp -= costCp; 

			if(aNewPosition != null)
			{
				var moveToSyncEnumerator = aCaster.MoveToSync(aNewPosition.Value, null);
				while(moveToSyncEnumerator.MoveNext()) yield return moveToSyncEnumerator.Current;
				if(aTargetCharacter != null) aCaster.LookAt(aTargetCharacter);
			}

			float timer = 0;
			m_OnCast.Invoke();
			using (var e = GetAttackKeyPointEnumerator())
			{
				while (e.MoveNext())
				{
					var attackPoint = e.Current;
					if (attackPoint == null) continue;
					var offsetTime = attackPoint.offsetTime;
					while (timer < offsetTime)
					{
						yield return null;
						timer += Time.deltaTime;
					}
					timer -= offsetTime;
					var castSyncEnumerator = attackPoint.CastSync(aBattleCore, aCaster, aTargetCharacter, aTargetPosition);
					if(castSyncEnumerator != null)
					{
						while (castSyncEnumerator.MoveNext())
						{
							yield return castSyncEnumerator.Current;
						}
					}
				}
			}
		}

		public bool SampleCast(BattleCharacter aCaster, 
			BattleCharacter aTargetCharacter, Vector2 aTargetPosition, out Vector2? aNewPosition)
		{
			aNewPosition = null;
			if (m_CastType == null) return false;
			return m_CastType.SampleVaildCastTarget(aTargetPosition, aTargetCharacter, aCaster, out aNewPosition);
		}

		public string skillName { get { return m_SkillName; } }
		public uint costCp { get { return m_CostCp; } }
		public int delayTime { get { return m_Delay; } }
		public float baseTime { get { return m_BaseTime; } }
		public CraftCastTypeSetting castType {get { return m_CastType; } }

		private struct AttackKeyPointEnumerator : IEnumerator<AttackKeyPoint>
		{
			internal AttackKeyPointEnumerator(CraftSetting aSkillSetting)
			{
				m_AttackKeyPoints = aSkillSetting.m_AttackKeyPoints;
				m_Index = -1;
			}

			public bool MoveNext()
			{
				if (m_AttackKeyPoints == null) return false;
				if (m_Index + 1 < m_AttackKeyPoints.Length)
				{
					m_Index++;
					return true;
				}
				return false;
			}

			public void Reset()
			{
				m_Index = -1;
			}

			public void Dispose() {}

			public AttackKeyPoint Current => m_AttackKeyPoints[m_Index];

			private readonly AttackKeyPoint[] m_AttackKeyPoints;
			private int m_Index;

			object IEnumerator.Current => Current;
		}

		private AttackKeyPointEnumerator GetAttackKeyPointEnumerator() => new AttackKeyPointEnumerator(this);

#pragma warning disable 0649
		[SerializeField] private string m_SkillName;
		[SerializeField] private uint m_CostCp;
		[SerializeField] private CraftCastTypeSetting m_CastType;
		[SerializeField] private int m_Delay;
		[SerializeField] private float m_BaseTime;
		[SerializeField] private AttackKeyPoint[] m_AttackKeyPoints;
		[SerializeField] private UnityEvent m_OnCast;
#pragma warning restore 0649

		//IExecutor IExecutorManager.GetExecutor(object aParam) => GetExecutor((ExecutorParam)aParam);
	}
}

