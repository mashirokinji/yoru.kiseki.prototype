﻿using System.Collections;
using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = AttackKeyPointAction.CREATE_ASSET_MENU_PATH + CREATE_ASSET_MENU_PATH)]
	public sealed class AttackKeyPointAction_PhysicalAttack : AttackKeyPointAction
	{
		public new const string CREATE_ASSET_MENU_PATH = "Physical attack";

		public override IEnumerator PlaySync(BattleCore aBattleCore, BattleCharacter aCaster, BattleCharacter aTarget)
		{
			uint value = (uint)((aCaster.str * m_CasterStrRatio) - (aTarget.def * m_TargetDefRatio));
			aTarget.hp -= value;
			return null;
		}

		[SerializeField] private float m_CasterStrRatio = 1;
		[SerializeField] private float m_TargetDefRatio = 1;
	}
}
