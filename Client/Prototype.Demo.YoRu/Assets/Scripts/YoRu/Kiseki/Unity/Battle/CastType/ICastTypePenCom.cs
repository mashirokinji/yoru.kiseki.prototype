namespace YoRu.Kiseki.Unity
{
	public interface ICastTypePenCom
	{
		void DrawCastCircle(YrCircle aCircle);
		void DrawAttackCircle(YrCircle aCircle);
	}

}