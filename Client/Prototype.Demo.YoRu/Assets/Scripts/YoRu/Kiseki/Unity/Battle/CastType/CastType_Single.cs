using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[System.Serializable]
	public sealed class CastType_Single : CastType
	{
		public override void DrawRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster)
		{
			if(aPen == null) return;
			if(aCaster == null) return;
			aPen.DrawCastCircle(new YrCircle(aCaster.position, m_CanMove ? m_Rng + aCaster.mov : m_Rng));
		}

		public override void DrawAttackRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster, ICastTypeUnitCom aTarget, Vector2 aPosition)
		{
			if(aPen == null) return;
			if(aTarget == null) return;
			aPen.DrawAttackCircle(new YrCircle(aTarget.position, aTarget.radius));
		}

		public override void Sample<T>(Vector2 aPositionParam, T aBattleCharacterParam,
			T aCaster, ReadOnlyList<T> aBackupUnit, ref BattleCastResult<T> aResult)
		{
			aResult.Normalized();
			if (!(aResult.isVaild = SampleVaildCastTarget(aPositionParam, aBattleCharacterParam, aCaster,
				out Vector2? aNewPosition))) return;
			aResult.effectBattleCharacterBuffer.Add(aBattleCharacterParam);
		}

		public override bool SampleVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster, out Vector2? aNewPosition)
		{
			aNewPosition = null;
			if (!m_BattleTargetType.IsMeet(aCaster, aUnitParam)) return false;
			if (!aCaster.SampleAttack(aUnitParam.position, aUnitParam.radius, m_CanMove, m_Rng, out aNewPosition)) return false;
			return true;
		}

		public override CastTypeParamType paramType => CastTypeParamType.CHARACTER;

#pragma warning disable 0649
		[SerializeField] private bool m_CanMove;
		[SerializeField] private uint m_Rng;
		[SerializeField] private BattleTargetType m_BattleTargetType;
#pragma warning restore 0649
	}

}