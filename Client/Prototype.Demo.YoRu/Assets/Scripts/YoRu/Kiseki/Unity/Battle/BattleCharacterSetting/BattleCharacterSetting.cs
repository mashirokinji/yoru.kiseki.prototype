﻿using System.Linq;
using UnityEngine;
using System;
using UnityEngine.AI;
using UnityEngine.Events;
using YoRu.Unity;
using System.Collections;
using System.Collections.Generic;

namespace YoRu.Kiseki.Unity
{
	[RequireComponent(typeof(NavMeshAgent))]
	[RequireComponent(typeof(NavMeshObstacle))]
	[DisallowMultipleComponent]
	public sealed class BattleCharacterSetting : BattleUnitSetting
	{
		public const float ALLOW_POSITION_DEVIATION = 0.1f;

		// When target position is invaild, try position whose delta of degree value.
		public const float SAMPLE_POSITION_RADIAN_DELTA = Mathf.PI * 0.1f;

		// When there aren't vaild position around target position, next find range delta value.
		public const float SAMPLE_POSITION_DISTANCE_DELTA = 0.2f;

		//public Vector2 SamplePosition(Vector2 aTarget)
		//{
		//	if (IsVaildMovePosition(aTarget)) return aTarget;

		//	Vector2 output = aTarget;
		//	Vector2 targetSelfNormalized = (position - aTarget).normalized;
		//	float targetSelfSqrDistance = (position - aTarget).sqrMagnitude;

		//	float searchingDistance = 0;

		//	while (true)
		//	{
		//		searchingDistance += SAMPLE_POSITION_DISTANCE_DELTA;
		//		if (searchingDistance.Square() > targetSelfSqrDistance) break;
		//		for (float angle = 0; angle < YrMath.FULL_ANGLE_DEGREE; angle += SAMPLE_POSITION_RADIAN_DELTA)
		//		{
		//			output = aTarget + (searchingDistance * targetSelfNormalized).Rotate(angle);
		//			if (IsVaildMovePosition(output)) return output;
		//		}
		//	}
		//	return aTarget;
		//}

		/// <summary>
		/// このキャラクター今回移動可能な有効なポイントを獲得します.
		/// </summary>
		/// <returns>The position.</returns>
		/// <param name="aTarget">目標</param>
		/// <param name="bestDistance">目標との最高距離</param>
		public Vector2 SamplePosition(Vector2 aTarget, float bestDistance)
		{
			Vector2 selfPosition = position;
			Vector2 output = aTarget;
			Vector2 targetSelfOffset = (selfPosition - aTarget);
			Vector2 targetSelfNormalized = targetSelfOffset.normalized;
			float targetSelfSqrDistance = targetSelfOffset.sqrMagnitude;
			float targetSelfDistance = targetSelfSqrDistance.Root();

			Vector2 bestPosition;
			if (m_Mov >= targetSelfDistance - bestDistance)
			{
				bestPosition = aTarget + targetSelfNormalized * bestDistance;
				if (IsVaildMovePosition(bestPosition)) return bestPosition;

				// try search a vaild position inside best distance
				var maxSearchDistance = Mathf.Abs(targetSelfDistance - m_Mov - bestDistance);
				for (float searchingDistance = SAMPLE_POSITION_DISTANCE_DELTA;
					 searchingDistance <= maxSearchDistance;
					 searchingDistance += SAMPLE_POSITION_DISTANCE_DELTA)
				{
					float maxCos = searchingDistance / (bestDistance * 2);
					float curRadian = YrMath.ZERO_ANGLE_RADIAN;
					float curCos = Mathf.Cos(curRadian);
					float deltaRadian = Mathf.Approximately(searchingDistance, 0) ? Mathf.PI : SAMPLE_POSITION_RADIAN_DELTA / searchingDistance;
					while (curCos >= maxCos && curRadian < YrMath.FULL_ANGLE_RADIAN)
					{
						var ti = targetSelfNormalized * searchingDistance;
						var po = bestPosition - ti.Rotate(curRadian);
						if (IsVaildMovePosition(po)) return po;

						if (!Mathf.Approximately(curCos, 1))
						{
							var no = bestPosition - ti.Rotate(-curRadian);
							if (IsVaildMovePosition(no)) return no;
						}
						curRadian += deltaRadian;
						curCos = Mathf.Cos(curRadian);
					}
				}

				// can't find a vaild position inside best distance, try find the closet position from target
				maxSearchDistance = (bestPosition - selfPosition).magnitude;
				for (float searchingDistance = SAMPLE_POSITION_DISTANCE_DELTA;
					 searchingDistance < maxSearchDistance;
					 searchingDistance += SAMPLE_POSITION_DISTANCE_DELTA)
				{
					float maxCos = searchingDistance / (bestDistance * 2);
					float curRadian = YrMath.STRAIGHT_ANGLE_RADIAN;
					float curCos = Mathf.Cos(curRadian);
					float deltaRadian = Mathf.Approximately(searchingDistance, 0) ? Mathf.PI : SAMPLE_POSITION_RADIAN_DELTA / searchingDistance;
					while (curCos <= maxCos && curRadian < YrMath.FULL_ANGLE_RADIAN)
					{
						var ti = targetSelfNormalized * searchingDistance;
						var po = bestPosition - ti.Rotate(curRadian);
						if (IsVaildMovePosition(po)) return po;

						if (!Mathf.Approximately(curCos, -1))
						{
							var no = bestPosition - ti.Rotate(-curRadian);
							if (IsVaildMovePosition(no)) return no;
						}
						curRadian += deltaRadian;
						curCos = Mathf.Cos(curRadian);
					}
				}
			}
			else
			{
				// This character isn't able to close target position, just move to the closet position from target position
				bestPosition = selfPosition - targetSelfNormalized * m_Mov;
				if (IsVaildMovePosition(bestPosition)) return bestPosition;

				var maxSearchDistance = m_Mov;
				for (float searchingDistance = SAMPLE_POSITION_DISTANCE_DELTA;
					 searchingDistance <= maxSearchDistance;
					 searchingDistance += SAMPLE_POSITION_DISTANCE_DELTA)
				{
					float maxCos = searchingDistance / (m_Mov * 2);
					float curRadian = YrMath.ZERO_ANGLE_RADIAN;
					float curCos = Mathf.Cos(curRadian);
					float deltaRadian = Mathf.Approximately(searchingDistance, 0) ? Mathf.PI : SAMPLE_POSITION_RADIAN_DELTA / searchingDistance;
					while (curCos >= maxCos && curRadian < YrMath.FULL_ANGLE_RADIAN)
					{
						var ti = targetSelfNormalized * searchingDistance;
						var po = bestPosition + ti.Rotate(curRadian);
						if (IsVaildMovePosition(po)) return po;

						if (!Mathf.Approximately(curCos, 1))
						{
							var no = bestPosition + ti.Rotate(-curRadian);
							if (IsVaildMovePosition(po)) return no;
						}
						curRadian += deltaRadian;
						curCos = Mathf.Cos(curRadian);
					}
				}
			}
			return aTarget;
		}

		public bool IsVaildMovePosition(Vector2 aPosition)
		{
			var selfPosition = position;
			if ((aPosition - selfPosition).sqrMagnitude > m_Mov.Square()) return false;

			var targetRange = new YrCircle(aPosition, radius);

			foreach (var bc in ALL)
			{
				if (bc == this) continue;
				var bcRange = new YrCircle(bc.position, bc.radius);
				if (bcRange.Contain(targetRange)) return false;
			}

			NavMeshPath path = new NavMeshPath();
			if (!NavMesh.CalculatePath(selfPosition.ToX0Y(), aPosition.ToX0Y(), NavMesh.AllAreas, path)) return false;
			if (path.status != NavMeshPathStatus.PathComplete) return false;

			var cs = path.corners;
			var lastPoint = cs.Last();
			if ((lastPoint.ToXZ() - aPosition).sqrMagnitude > ALLOW_POSITION_DEVIATION) return false;

			return true;
		}

		public void MoveTo(Vector2 aPosition, Action aCallback)
		{
			StartCoroutine(MoveToSync(aPosition, aCallback));
		}

		public IEnumerator MoveToSync(Vector2 aPosition, Action aCallback)
		{
			if (isBusy)
			{
				aCallback?.Invoke();
				yield break;
			}

			if (!IsVaildMovePosition(aPosition))
			{
				aCallback?.Invoke();
				yield break;
			}

			isBusy = true;

			m_StateInfo = new StateInfo(BattleCharacterState.MOVEING);
			onStartMove?.Invoke();

			m_NavMeshAgent.enabled = true;
			m_NavMeshAgent.SetDestination(aPosition.ToX0Y());

			float sqrMov = m_Mov.Square();
			while (true)
			{
				DebugEx.DrawPath(m_NavMeshAgent.path, Color.red);
				if ((m_Transform.position - m_NavMeshAgent.destination).sqrMagnitude <= 0) break;
				yield return null;
			}

			m_NavMeshAgent.ResetPath();
			m_NavMeshAgent.enabled = false;
			onEndMove?.Invoke();
			aCallback?.Invoke();
		}

		public bool CanAttack(BattleCharacterSetting aTarget, out Vector2? aNewPosition)
		{
			aNewPosition = null;
			if (aTarget == null) return false;
			if (aTarget == this) return false;
			YrCircle targetCircle = new YrCircle(aTarget.position, radius);
			YrCircle origiAttack = new YrCircle(position, m_Rng);
			if (origiAttack.Contain(targetCircle)) return true;
			aNewPosition = SamplePosition(aTarget.position, aTarget.radius + rng);
			YrCircle npAttackRng = new YrCircle(aNewPosition.Value, rng);
			return npAttackRng.Contain(targetCircle);
		}

		public void Attack(BattleCharacterSetting aTarget, Action aCallback)
		{
			if (aTarget == this)
			{
				aCallback?.Invoke();
				return;
			}

			if (isBusy)
			{
				aCallback?.Invoke();
				return;
			}

			if (CanAttack(aTarget, out Vector2? newPosition))
			{
				isBusy = true;
				// delay += (int)(10 * (100 - spd / 100.0f));
				Action attackCallback = () =>
				{
					aCallback?.Invoke();
					isBusy = false;
				};
				if (newPosition == null)
				{
					StartCoroutine(AttackSync(aTarget, attackCallback));
				}
				else
				{
					StartCoroutine(MoveToSync(newPosition.Value, () =>
					{
						StartCoroutine(AttackSync(aTarget, attackCallback));
					}));
				}
			}
			else
			{
				MoveTo(newPosition == null ? position : newPosition.Value, aCallback);
			}
		}

		public void Hurt(Vector2 aDirection)
		{
			if (isBusy) return;
			isBusy = true;
			m_OnStartHurt?.Invoke();
			isBusy = false;
		}

		public void Die(Action aCallback)
		{
			isBusy = true;
			StartCoroutine(DieSync(() =>
				{
					isBusy = false;
					aCallback?.Invoke();
				}));
		}

		public void LookAt(BattleCharacterSetting aCharacter)
		{
			if(aCharacter == null) return;
			Vector2 direction = aCharacter.position - position;
			m_Transform.rotation = Quaternion.LookRotation(direction.ToX0Y());
		}

		public CraftSetting GetCraftIdSetting(int aIndex)
		{
			if(m_CraftSetting == null) return null;
			if(aIndex < 0 || aIndex >= m_CraftSetting.Length) return null;
			return m_CraftSetting[aIndex];
		}

		public event Action onStartMove;
		public event Action onEndMove;
		public event Action onStartAttack;
		public event Action onEndAttack;

		public int craftCount { get { return m_CraftSetting.Length; } }

		public BattleCharacterState state { get { return m_StateInfo.state; } }
		public byte teamId => m_TeamId;
		public float stateTimer { get { return m_StateInfo.timer; } }
		public bool isBusy { get; private set; }

		public Vector2 forward
		{
			get { return m_Transform.rotation.eulerAngles.ToXZ(); }
			set
			{
				m_Transform.rotation = Quaternion.Euler(value.ToX0Y());
			}
		}
		public Vector2 position
		{
			get { return m_Transform.position.ToXZ(); }
			set
			{
				if (NavMesh.SamplePosition(value.ToX0Y(), out NavMeshHit hit, float.PositiveInfinity, NavMesh.AllAreas))
				{
					m_Transform.position = hit.position;
				}
			}
		}
		public Vector3 velocity { get { return m_NavMeshAgent.velocity; } }

		public float radius { get { return m_NavMeshAgent.radius; } }

		public uint maxHp
		{
			get { return m_MaxHp; }
			set { m_MaxHp = Math.Max(value, 0); }
		}
		public uint hp
		{
			get { return m_Hp; }
			set
			{
				m_Hp = Math.Min(m_MaxHp, Math.Max(value, 0));
			}
		}
		public uint str { get { return m_Str; } }
		public uint def { get { return m_Def; } }
		public uint spd { get { return m_Spd; } }
		public uint rng { get { return m_Rng; } }
		public uint mov { get { return m_Mov; } }

		[Serializable]
		private struct DieSetting
		{
			public float baseTime => m_BaseTime;
			public void InvokeOnStart() => m_OnStart.Invoke();
			public void InvokeOnEnd() => m_OnEnd.Invoke();

#pragma warning disable 0649
			[SerializeField] private float m_BaseTime;
			[SerializeField] private UnityEvent m_OnStart;
			[SerializeField] private UnityEvent m_OnEnd;
#pragma warning restore 0649
		}

		private struct StateInfo
		{
			public StateInfo(BattleCharacterState aState)
			{
				state = aState;
				timer = 0;
			}
			public BattleCharacterState state { get; }
			public float timer { get; set; } // recoard the time start from current state
		}

		private void Awake()
		{
			m_Transform = GetComponent<Transform>();
			m_NavMeshAgent = GetComponent<NavMeshAgent>();
			m_NavMeshObstacle = GetComponent<NavMeshObstacle>();
		}

		private void Start()
		{
			//m_NavMeshAgent.enabled = false;
			//m_NavMeshObstacle.enabled = false;
		}

		private void OnEnable()
		{
			ALL.Add(this);
		}

		private void OnDisable()
		{
			ALL.Remove(this);
		}

		private void Update()
		{
			m_StateInfo.timer += Time.deltaTime;
			//m_NavMeshAgent.isStopped
		}

		private IEnumerator AttackSync(BattleCharacterSetting aCharacter, Action aCallback)
		{
			// yield return new WaitForSeconds(1);
			m_StateInfo = new StateInfo(BattleCharacterState.ATTACKING);
			LookAt(aCharacter);
			onStartAttack?.Invoke();
			m_OnStartAttack?.Invoke();

			int keyPointLength = m_AttackSetting.attackKeyPointLength;
			int index = 0;
			while (keyPointLength > index)
			{
				var point = m_AttackSetting.GetAttackKeyPoint(index);
				if (point.offsetTime <= stateTimer)
				{
					// using (var e = point.GetActionEnumerator())
					// {
					// 	while (e.MoveNext())
					// 	{
					// 		var action = e.Current;
					// 		if (action == null) continue;
							// action.GetExecuter(new BaseAttackKeyPointActionExecuter.InitParam
							// {
							// 	caster = this
							// });
							yield return null;
					// 	}
					// }
					index++;
					continue;
				}
				yield return null;
			}

			onEndAttack?.Invoke();
			m_OnEndAttack?.Invoke();
			aCallback?.Invoke();
		}

		private IEnumerator DieSync(Action aCallback)
		{
			m_DieSetting.InvokeOnStart();
			yield return WaitForSecondsEx.Get(m_DieSetting.baseTime);
			m_DieSetting.InvokeOnEnd();
			aCallback?.Invoke();
		}

		private static readonly List<BattleCharacterSetting> ALL = new List<BattleCharacterSetting>();
		private StateInfo m_StateInfo;
		private NavMeshAgent m_NavMeshAgent;
		private NavMeshObstacle m_NavMeshObstacle;
		private Transform m_Transform;

#pragma warning disable 0649
		[SerializeField] private AttackSetting m_AttackSetting;
		[SerializeField] private DieSetting m_DieSetting;
		[SerializeField] private CraftSetting[] m_CraftSetting;

		[SerializeField] private byte m_TeamId;
		[SerializeField] private uint m_MaxHp;
		[SerializeField] private uint m_Hp;
		[SerializeField] private uint m_Str;
		[SerializeField] private uint m_Def;
		[SerializeField] private uint m_Mov;
		[SerializeField] private uint m_Spd;
		[SerializeField] private uint m_Rng;

		[SerializeField] private UnityEvent m_OnStartAttack;
		[SerializeField] private UnityEvent m_OnEndAttack;

		[SerializeField] private UnityEvent m_OnStartHurt;
		[SerializeField] private UnityEvent m_OnEndHurt;
#pragma warning restore 0649
	}
}

