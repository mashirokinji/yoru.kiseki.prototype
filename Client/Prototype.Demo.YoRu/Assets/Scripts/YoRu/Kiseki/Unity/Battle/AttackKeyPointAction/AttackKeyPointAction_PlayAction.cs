using System.Collections;
using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = AttackKeyPointAction.CREATE_ASSET_MENU_PATH + CREATE_ASSET_MENU_PATH)]
	public sealed class AttackKeyPointAction_PlayAction : AttackKeyPointAction
	{
		private AttackKeyPointAction_PlayAction() { }

		public new const string CREATE_ASSET_MENU_PATH = "PlayAction";

		public override IEnumerator PlaySync(BattleCore aBattleCore, BattleCharacter aCaster, BattleCharacter aTarget)
		{
			aTarget.Hurt((aTarget.position - aCaster.position).normalized);
			if (debug) Debug.Log(aTarget.name + " play hurt.");
			return null;
		}
	}
}
