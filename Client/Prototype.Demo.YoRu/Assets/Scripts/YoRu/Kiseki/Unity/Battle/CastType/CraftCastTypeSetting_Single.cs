﻿using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = CREATE_ASSET_MENU_PATH + "Single")]
	public sealed class CraftCastTypeSetting_Single : CriftCastTypeSetting<CastType_Single> { }
}