﻿using System;
using UnityEditor;
using UnityEngine;
using YoRu.Unity;
using YoRu.Unity.Editor;
using System.Collections.Generic;

namespace YoRu.Kiseki.Unity
{
	[CustomEditor(typeof(BattleCore))]
	public class BattleCoreEditor : Editor
	{
		public abstract class BaseState
		{
			public virtual void DrawInspectorGui() { }
			public virtual void DrawSceneGui() { }
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			state?.DrawInspectorGui();
			Repaint();
		}

		public void ResetState()
		{
			state = m_WaitingCommandState;
		}

		public BaseState state { get; set; }

		private void OnEnable()
		{
			if(m_WaitingCommandState == null)
			{
				m_WaitingCommandState = new WaitingCommandState(this);
				state = m_WaitingCommandState;
			}
			Tools.hidden = true;
			SceneView.onSceneGUIDelegate += SceneView_OnSceneGUIDelegate;
		}

		private void OnDisable()
		{
			Tools.hidden = false;
			SceneView.onSceneGUIDelegate -= SceneView_OnSceneGUIDelegate;
		}

		private void SceneView_OnSceneGUIDelegate(SceneView sceneView)
		{
			state?.DrawSceneGui();
		}

		private List<BattleCharacter> m_PopupBuffer = new List<BattleCharacter>();
		private int m_PopupIndex;
		private BattleCharacter m_Target;
		private Vector3 position { get; set; }
		private bool m_CanAttack;
		private Vector2? m_NewPosition;
		private WaitingCommandState m_WaitingCommandState;
	}

	public class WaitingCommandState : BattleCoreEditor.BaseState
	{
		public WaitingCommandState(BattleCoreEditor aOwner)
		{
			m_Owner = aOwner;
			m_UsingCraftSKillState = new UsingCraftSkillState(m_Owner);
		}

		public override void DrawInspectorGui()
		{
			var bc = (BattleCore) m_Owner.target;
			if (GUILayout.Button("Move")) throw new NotImplementedException();
			if (GUILayout.Button("Attack")) throw new NotImplementedException(); 
			if (GUILayout.Button("Crift")) m_Owner.state = m_UsingCraftSKillState; 
		}

		public readonly BattleCoreEditor m_Owner;
		private readonly UsingCraftSkillState m_UsingCraftSKillState;
	}

	// private class WaitingSelectMovePositionState : BaseState
	// {
	// 	public WaitingSelectMovePositionState(BattleCoreEditor aOwner, BaseState aNextState)
	// 	{
	// 		m_Owner = aOwner;
	// 	}

	// 	public override void DrawInspectorGUI()
	// 	{
	// 		if (GUILayout.Button("Confirm"))
	// 		{
	// 			m_Owner.m_State = null;
	// 			BattleCore bc = (BattleCore)m_Owner.target;
	// 			bc.MoveCharacter(bc.currentUnit, m_Owner.position.ToXZ(), () =>
	// 				{
	// 					m_Owner.m_State = m_NextState;
	// 				});
	// 		}
	// 	}

	// 	public override void DrawSceneGui()
	// 	{
	// 		var bc = (BattleCore) m_Owner.target;
	// 		var currentUnit = bc.currentUnit;
	// 		if (currentUnit == null) return;
	// 		m_Owner.position = Handles.PositionHandle(m_Owner.position, Quaternion.identity);
	// 		HandlesEx.DrawWithColor(currentUnit.IsVaildMovePosition(m_Owner.position.ToXZ()) ? Color.cyan : Color.red, () =>
	// 		{
	// 			Handles.DrawSolidDisc(m_Owner.position, Vector3.up, currentUnit.radius);
	// 		});

	// 		HandlesEx.DrawWithColor(Color.cyan * 0.3f, () =>
	// 		{
	// 			Handles.DrawSolidDisc(currentUnit.position.ToX0Y(), Vector3.up, currentUnit.mov);
	// 		});
	// 	}

	// 	private readonly BattleCoreEditor m_Owner;
	// 	private readonly BaseState m_NextState;
	// }

	// private class WaitingSelectBattleCharacterState : BattleCoreEditor.BaseState
	// {
	// 	public WaitingSelectBattleCharacterState(BattleCoreEditor aOwner, BaseState aNextState, Func<bool> aFilter)
	// 	{
	// 		m_Owner = aOwner;
	// 	}

	// 	public override void DrawInspectorGui()
	// 	{
	// 	}

	// 	public override void DrawSceneGui()
	// 	{
	// 	}

	// 	private readonly BattleCoreEditor m_Owner;
	// 	private readonly BaseState m_NextState;
	// 	private readonly Func<bool> m_Filter;
	// }

	public class UsingCraftSkillState : BattleCoreEditor.BaseState, ICastTypePenCom
	{
		public UsingCraftSkillState(BattleCoreEditor aOwner)
		{
			m_BattleCoreEditor = aOwner;
			m_WaitingSelectCraft = new WaitingSelectCraft(this);
			state = m_WaitingSelectCraft;
		}

		public class BaseState
		{
			public virtual void OnEnter() { }
			public virtual void OnExit() { }
			public virtual void DrawInspectorGUI() { }
			public virtual void DrawSceneGui() { }
		}

		public override void DrawInspectorGui() => state?.DrawInspectorGUI();

		public override void DrawSceneGui() => state?.DrawSceneGui();

		public void Exit()
		{
			state = m_WaitingSelectCraft;
			m_BattleCoreEditor.ResetState();
		}

		public BattleCore battleCore { get { return m_BattleCoreEditor.target as BattleCore; } }
		public BaseState state 
		{ 
			get { return m_BaseState; } 
			set 
			{ 
				if(m_BaseState == value) return;
				m_BaseState?.OnExit();
				m_BaseState = value;
				m_BaseState?.OnEnter();
			} 
		}
		public int craftIndex { get; set; }

		private readonly BattleCoreEditor m_BattleCoreEditor;
		private readonly WaitingSelectCraft m_WaitingSelectCraft;
		private BaseState m_BaseState;

		void ICastTypePenCom.DrawCastCircle(YrCircle aCircle)
		{
			HandlesEx.DrawWithColor(ColorEx.orange * 0.3f, () =>
			{
				Handles.DrawSolidDisc(aCircle.center.ToX0Y(), Vector3.up, aCircle.radius);
			});
		}

		void ICastTypePenCom.DrawAttackCircle(YrCircle aCircle)
		{
			HandlesEx.DrawWithColor(ColorEx.orange * 0.3f, () =>
			{
				Handles.DrawSolidDisc(aCircle.center.ToX0Y(), Vector3.up, aCircle.radius);
			});
		}
	}

	public class WaitingSelectCraft : UsingCraftSkillState.BaseState
	{
		public WaitingSelectCraft(UsingCraftSkillState aOwner)
		{
			m_Owner = aOwner;
			m_WaitingSelectCraftTargetByPosition = new WaitingSelectCraftTargetByPosition(m_Owner);
			m_WaitingSelectCraftTargetByCharacter = new WaitingSelectCraftTargetByCharacter(m_Owner);
		}

		public override void DrawInspectorGUI()
		{
			BattleCore bc = m_Owner.battleCore;
			if(bc == null) return;
			var unit = bc.currentUnit;
			if(unit == null) return;
			for (int i = 0; i < unit.craftCount; i++)
			{
				var craftSetting = unit.GetCraftSetting(i);
				if(craftSetting == null) continue;
				if (GUILayout.Button($"{craftSetting.skillName} cp:{craftSetting.costCp}"))
				{
					m_Owner.craftIndex = i;
					switch (craftSetting.castType.paramType)
					{
						case CastTypeParamType.NONE:
							break;
						case CastTypeParamType.POSITION:
							m_Owner.state = m_WaitingSelectCraftTargetByPosition;
							break;
						case CastTypeParamType.CHARACTER:
							m_Owner.state = m_WaitingSelectCraftTargetByCharacter;
							break;
					}
				}
			}
		}

		private readonly UsingCraftSkillState m_Owner;
		private readonly WaitingSelectCraftTargetByCharacter m_WaitingSelectCraftTargetByCharacter;
		private readonly WaitingSelectCraftTargetByPosition m_WaitingSelectCraftTargetByPosition;
	}

	public class WaitingSelectCraftTargetByPosition : UsingCraftSkillState.BaseState
	{
		public WaitingSelectCraftTargetByPosition(UsingCraftSkillState aOwner)
		{
			m_Owner = aOwner;
		}

		public override void DrawInspectorGUI()
		{
			var bc = m_Owner.battleCore;
			if(bc == null) return;

			var currentUnit = bc.currentUnit;
			if (currentUnit == null) return;

			var curCraftSetting = currentUnit.GetCraftSetting(m_Owner.craftIndex);
			if(curCraftSetting == null) return;

			var castType = curCraftSetting.castType;
			if(castType == null) return;

			if(castType.SampleVaildCastTarget(m_Position, null, currentUnit, 
				out Vector2? newPosition))
			{
				if(GUILayout.Button("Confirm"))
				{
					bc.UseCraft(currentUnit, m_Owner.craftIndex, m_Position, null, () =>
					{

					});
				}
			}
		}

		public override void DrawSceneGui()
		{
			var bc = m_Owner.battleCore;
			if(bc == null) return;

			var currentUnit = bc.currentUnit;
			if (currentUnit == null) return;

			var curCraftSetting = currentUnit.GetCraftSetting(m_Owner.craftIndex);
			if(curCraftSetting == null) return;

			var castType = curCraftSetting.castType;
			if(castType == null) return;

			m_Position = Handles.PositionHandle(m_Position.ToX0Y(), Quaternion.identity).ToXZ();

			castType.DrawRange(m_Owner, currentUnit);
			castType.DrawAttackRange(m_Owner, currentUnit, null, m_Position);

			if(castType.SampleVaildCastTarget(m_Position, null, currentUnit, 
				out Vector2? newPosition))
			{
				
			}
		}

		private UsingCraftSkillState m_Owner;
		private Vector2 m_Position;
	}

	public class WaitingSelectCraftTargetByCharacter : UsingCraftSkillState.BaseState
	{
		public WaitingSelectCraftTargetByCharacter(UsingCraftSkillState aOwner)
		{
			m_Owner = aOwner;
		}

		public override void OnEnter()
		{
			RefreshValidTarget();
			m_SelectCharacterIndex = 0;
		}

		public override void DrawInspectorGUI()
		{
			var bc = m_Owner.battleCore;
			if (bc == null) return;

			var currentUnit = bc.currentUnit;
			if (currentUnit == null) return;

			var curCraftSetting = currentUnit.GetCraftSetting(m_Owner.craftIndex);
			if (curCraftSetting == null) return;

			var castType = curCraftSetting.castType;
			if (castType == null) return;

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("<<"))
			{
				if (--m_SelectCharacterIndex < 0) m_SelectCharacterIndex = m_BattleCharacterBuffer.Count - 1;
			}
			if (GUILayout.Button(">>"))
			{
				if (++m_SelectCharacterIndex >= m_BattleCharacterBuffer.Count) m_SelectCharacterIndex = 0;
			}
			EditorGUILayout.EndHorizontal();

			if (m_BattleCharacterBuffer.TryGet(m_SelectCharacterIndex, out BattleCharacter targetBattleCharacter))
			{
				EditorGUILayout.LabelField(targetBattleCharacter.name);
				if (targetBattleCharacter != null)
				{
					HandlesEx.DrawWithColor(Color.blue , () =>
					{
						Handles.DrawSolidDisc(targetBattleCharacter.position.ToX0Y(), Vector3.up, 10);
					});
					if (castType.SampleVaildCastTarget(Vector2.zero, targetBattleCharacter, currentUnit, 
						out Vector2? newPosition))
					{
						if (GUILayout.Button("Confirm"))
						{
							bc.UseCraft(currentUnit, m_Owner.craftIndex, Vector2.zero, targetBattleCharacter, () =>
							{

							});
						}
					}
				}
			}
		}

		public override void DrawSceneGui()
		{
			var bc = m_Owner.battleCore;
			if(bc == null) return;

			var currentUnit = bc.currentUnit;
			if (currentUnit == null) return;

			var curCraftSetting = currentUnit.GetCraftSetting(m_Owner.craftIndex);
			if(curCraftSetting == null) return;

			var castType = curCraftSetting.castType;
			if(castType == null) return;

			castType.DrawRange(m_Owner, currentUnit);

			if (m_BattleCharacterBuffer.TryGet(m_SelectCharacterIndex, out BattleCharacter target))
			{
				if (target != null)
				{
					HandlesEx.DrawWithColor(Color.red * 0.3f, () =>
					{
						Handles.DrawSolidDisc(target.position.ToX0Y(), Vector3.up, target.radius);
					});
				}
			}
		}

		private void RefreshValidTarget()
		{
			m_BattleCharacterBuffer.Clear();

			if(m_Owner == null) return;

			var battleCore = m_Owner.battleCore;
			if(battleCore == null) return;

			var currentUnit = battleCore.currentUnit;
			if(currentUnit == null) return;

			var craftSetting = currentUnit.GetCraftSetting(m_Owner.craftIndex);
			if(craftSetting == null) return;

			var castType = craftSetting.castType;
			if(castType == null) return;

			using(var e = battleCore.GetAllCharacterEnumerator())
			{
				while(e.MoveNext())
				{
					var c = e.Current;
					if(c == null) continue;
					if(castType.IsVaildCastTarget(Vector2.zero, c, currentUnit)) m_BattleCharacterBuffer.Add(c);
				}
			}
		}

		private readonly UsingCraftSkillState m_Owner;
		private readonly List<BattleCharacter> m_BattleCharacterBuffer = new List<BattleCharacter>();
		private int m_SelectCharacterIndex;
	}
}