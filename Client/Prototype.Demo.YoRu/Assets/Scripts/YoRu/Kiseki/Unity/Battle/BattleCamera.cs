﻿using UnityEngine;
using YoRu.Kiseki.Unity;
using YoRu.Unity;

[RequireComponent(typeof(BattleCamera))]
public class BattleCamera : KisekiBehaviour
{
	public void ResetAngle()
	{
		currentVerticalAngle = m_VerticalAngle;
		currentHorizontalAngle = m_HorizontalAngle;
	}

	public Transform target { get { return m_Traget; } set { m_Traget = value; } }

	public bool canBeControlled { get { return m_CanBeControled; } set { m_CanBeControled = value; } }

	private void Awake()
	{
		m_Transform = GetComponent<Transform>();
		m_Camera = this.GetAddComponent<Camera>();
	}

	private void Start()
	{
		ResetAngle();
	}

	private void Update()
	{
		if(canBeControlled)
		{
			float cameraHorizontal = Input.GetAxis("CameraHorizontal");
			float cameraVertical = Input.GetAxis("CameraVertical");
			bool r1 = Input.GetButton("R1");
			bool l1 = Input.GetButton("L1");

			float zoom = 0;
			if (r1) zoom--;
			if (l1) zoom++;
			m_Distance = Mathf.Clamp(m_Distance + zoom * Time.deltaTime * m_Speed.z, m_MinDistance, m_MaxDistance);
			currentVerticalAngle = Mathf.Clamp(currentVerticalAngle + cameraVertical * Time.deltaTime * m_Speed.y, m_MinVerticalAngle, m_MaxVerticalAngle);
			currentHorizontalAngle = (currentHorizontalAngle + cameraHorizontal * Time.deltaTime * m_Speed.x) % 360;
		}

		if (target == null) return;

		Vector3 newPosition = m_Traget.position +
			Quaternion.Euler(m_Traget.rotation.eulerAngles + new Vector3(currentVerticalAngle, currentHorizontalAngle, 0)) *
			Vector3.back * m_Distance;
		// newPosition += m_Transform.forward * zoom * m_Speed.z * Time.deltaTime;
		Vector3 lerpPosition = Vector3.Lerp(m_Transform.position, newPosition, m_LerpSpeed);
		m_Transform.position = lerpPosition;
		m_Transform.LookAt(m_Traget.position);
	}

	private float currentVerticalAngle { get; set; }
	private float currentHorizontalAngle { get; set; }

	private Transform m_Transform;
	private Camera m_Camera;

#pragma warning disable 0649
	[SerializeField] private bool m_CanBeControled;
	[SerializeField] private Transform m_Traget;
	[SerializeField] private Vector3 m_Speed;
	[SerializeField, Range(0.1f, 1)] private float m_LerpSpeed = 0.3f;
	[SerializeField] private float m_Distance = 5;
	[SerializeField] private float m_MaxDistance = 10;
	[SerializeField] private float m_MinDistance = 1;
	[SerializeField] private float m_VerticalAngle = 20;
	[SerializeField] private float m_MaxVerticalAngle = 80;
	[SerializeField] private float m_MinVerticalAngle = 10;
	[SerializeField] private float m_HorizontalAngle;
#pragma warning restore 0649
}