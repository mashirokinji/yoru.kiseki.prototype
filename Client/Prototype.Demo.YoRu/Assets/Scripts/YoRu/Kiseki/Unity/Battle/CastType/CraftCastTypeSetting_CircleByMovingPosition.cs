﻿using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = CREATE_ASSET_MENU_PATH + "Cirlce by moving position")]
	public sealed class CraftCastTypeSetting_CircleByMovingPosition : CriftCastTypeSetting<CastType_CircleByMovingPosition> {}
}