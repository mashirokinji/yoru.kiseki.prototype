﻿using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[CreateAssetMenu(menuName = CREATE_ASSET_MENU_PATH + "All")]
	public sealed class CraftCastTypeSetting_All : CriftCastTypeSetting<CastType_All> { }
}