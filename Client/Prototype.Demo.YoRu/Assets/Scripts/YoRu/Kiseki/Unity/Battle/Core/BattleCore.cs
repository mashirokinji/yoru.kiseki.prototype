﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;

namespace YoRu.Kiseki.Unity
{
	public sealed class BattleCore : KisekiBehaviour
	{
		public struct AllCharacterEnumerator : IEnumerator<BattleCharacter>
		{
			internal AllCharacterEnumerator(BattleCore aBattleCore)
			{
				m_Enumerator = aBattleCore.m_CharacterHandlers.GetEnumerator();
			}

			public bool MoveNext() => m_Enumerator.MoveNext();

			public void Dispose() => m_Enumerator.Dispose();

			public BattleCharacter Current => m_Enumerator.Current;

			private List<BattleCharacter>.Enumerator m_Enumerator;

			object IEnumerator.Current => m_Enumerator.Current;

			void IEnumerator.Reset() => ((IEnumerator)m_Enumerator).Reset();
		}

		public AllCharacterEnumerator GetAllCharacterEnumerator() => new AllCharacterEnumerator(this);

		public void GetAllCharacters(ref List<BattleCharacter> aListBuffer)
		{
			if(aListBuffer == null)
			{
				aListBuffer = new List<BattleCharacter>();
			}
			else
			{
				aListBuffer.Clear();
			}

			foreach (var e in m_CharacterHandlers)
			{
				aListBuffer.Add(e);
			}
		}

		public void MoveCharacter(BattleCharacter aBattleCharacter, Vector2 aPosition, Action aCallback)
		{
			if (isBusy) return;
			aBattleCharacter.MoveTo(aPosition, aCallback);
		}

		public void Attack(BattleCharacter aCaster, BattleCharacter aTarget, Action aCallback)
		{
			if (isBusy) return;
			m_AttackCasterBuffer = aCaster;
			m_AttackTargetBuffer = aTarget;
			m_CommandCallback = aCallback;
			aCaster.Attack(aTarget, m_OnAttackCallback);
		}

		public void UseCraft(BattleCharacter aCaster, int aCraftId, Vector2 aPosition,
			BattleCharacter aTarget, Action aCallback)
		{
			if (aCaster == null) return;
			if (aTarget == null) return;
			var craftSetting = aCaster.GetCraftSetting(0);
			if (craftSetting == null) return;

			if (craftSetting.SampleCast(aCaster, aTarget, aPosition, out Vector2? aNewPosition))
			{
				StartCoroutine(UseCraftSync(aCraftId, aCaster, aTarget, aPosition, aCallback));
			}
			else
			{
				if (aNewPosition != null)
				{
					aCaster.MoveTo(aNewPosition.Value, aCallback);
				}
			}
		}

		public BattleCharacter currentUnit
		{
			get
			{
				return m_CharacterHandlers == null || m_CharacterHandlers.Count < 1 ? null : m_CharacterHandlers[0];
			}
		}

		public bool isBusy { get { return m_Executor != null; } }

		// private 
		private class InnerBattleCharacter : BattleCharacter
		{
			public InnerBattleCharacter(BattleCore aCore, BattleCharacterSetting aSetting) : base(aCore, aSetting) { }

			public override int delay { get { return innerDelay; } set { innerDelay = value; } }
			public int innerDelay { get; set; }
		}

		private void Awake()
		{
			m_Button1Listener = new Button1Listener();
			m_Button2Listener = new Button2Listener();
			m_CharacterHandlers = new List<BattleCharacter>();
			// m_BattleCharacterDict = new Dictionary<CharacterIndex, CharacterHandler>();

			m_OnAttackCallback = () =>
			{
				if (m_AttackTargetBuffer.hp <= 0)
				{
					m_AttackTargetBuffer.Die(m_OnAttackTagetDieCallback);
				}
				else
				{
					m_CommandCallback?.Invoke();
					m_CommandCallback = null;
					m_AttackCasterBuffer = null;
					m_AttackTargetBuffer = null;
				}
			};

			m_OnAttackTagetDieCallback = () =>
			{
				m_CommandCallback?.Invoke();
				m_CommandCallback = null;
				m_AttackCasterBuffer = null;
				m_AttackTargetBuffer = null;
			};

			m_OnMoveToCallback = () =>
			{
				m_CommandCallback?.Invoke();
				m_CommandCallback = null;
			};
		}

		private void Start()
		{
			foreach (var battleCharacter in m_BattleCharacters)
			{
				var bc = new InnerBattleCharacter(this, battleCharacter);
				m_CharacterHandlers.Add(bc);
			}
		}

		private void Update()
		{
			m_Button1Listener.Update();
			m_Button2Listener.Update();

			if (isBusy)
			{
				Update_Idle();
			}
			else
			{
				Update_Busing();
			}
		}

		private void Update_Idle()
		{
			var cu = (InnerBattleCharacter)currentUnit;
			while (cu.delay != 0)
			{
				int reduceDelay = cu.delay;

				foreach (var characterHandler in m_CharacterHandlers)
				{
					cu.innerDelay -= reduceDelay;
				}

				m_CharacterHandlers.Sort((x, y) =>
				{
					return x.delay.CompareTo(y.delay);
				});

				m_Camera.target = cu.transform;
			}
		}

		private void Update_Busing()
		{
			// if(!m_Executor.MoveNext()) return;
			// m_CommandCallback?.Invoke();
			// m_ExecutorManager.RecycleExecutor(m_Executor);
			// m_Executor = null;
			// m_ExecutorManager = null;
		}

		private IEnumerator UseCraftSync(int aIndex, BattleCharacter aCaster, BattleCharacter aTarget, Vector2 aPosition,
			Action aCallback)
		{
			if (aCaster == null) goto end;
			if (aTarget == null) goto end;
			var craftSetting = aCaster.GetCraftSetting(0);
			if (craftSetting == null) goto end;

			var castSyncEnumerator = craftSetting.CastSync(this, aCaster, aTarget, aPosition);
			if (castSyncEnumerator != null)
			{
				while (castSyncEnumerator.MoveNext())
				{
					yield return castSyncEnumerator.Current;
				}
			}

			end:
				aCallback?.Invoke();
		}

		private Button1Listener m_Button1Listener;
		private Button2Listener m_Button2Listener;

		private CraftSetting m_CraftSetting;
		private BattleCharacter m_AttackCasterBuffer;
		private BattleCharacter m_AttackTargetBuffer;

		private Action m_OnAttackCallback;
		private Action m_OnAttackTagetDieCallback;
		private Action m_OnMoveToCallback;

		// handle the callback from other class when call battle command like Attack, MoveCharacter 
		private Action m_CommandCallback;

		private IExecutorManager m_ExecutorManager;
		private IExecutor m_Executor;

		private List<BattleCharacter> m_CharacterHandlers;

#pragma warning disable 0649
		[SerializeField] private UnityEvent m_OnStartWaitCommand;
		[SerializeField] private UnityEvent m_OnEndWaitCommand;
		[SerializeField] private BattleCursor m_Cursor;
		[SerializeField] private BattleCamera m_Camera;
		[SerializeField] private BattleCharacterSetting[] m_BattleCharacters;
#pragma warning restore 0649
	}
}
