using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	public abstract class CastTypeSetting : KisikiScriptableObject
	{
		public new const string CREATE_ASSET_MENU_PATH = KisikiScriptableObject.CREATE_ASSET_MENU_PATH + "Cast type/";

		public void DrawRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster) => castType.DrawRange(aPen, aCaster);

		public void DrawAttackRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster, ICastTypeUnitCom aTarget, Vector2 aPosition) =>
			castType.DrawAttackRange(aPen, aCaster, aTarget, aPosition);

		public void Sample<T>(Vector2 aPositionParam, T aBattleCharacterParam,
			T aCaster, ReadOnlyList<T> aBackupUnit, ref BattleCastResult<T> aResult) where T : ICastTypeUnitCom =>
			castType.Sample<T>(aPositionParam, aBattleCharacterParam, aCaster, aBackupUnit, ref aResult);

		public bool SampleVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster, out Vector2? aNewPosition) => 
			castType.SampleVaildCastTarget(aPositionParam, aUnitParam, aCaster, out aNewPosition);

		public bool IsVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster) => 
			castType.SampleVaildCastTarget(aPositionParam, aUnitParam, aCaster, out Vector2? newPosition);

		public CastTypeParamType paramType { get { return castType.paramType; } }

		protected abstract CastType castType { get; }
	}
}