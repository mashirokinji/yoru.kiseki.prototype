using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	[System.Serializable]
	public sealed class CastType_All : CastType
	{
		public override void DrawRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster)
		{
			throw new System.NotImplementedException();
		}

		public override void DrawAttackRange(ICastTypePenCom aPen, ICastTypeUnitCom aCaster, ICastTypeUnitCom aTarget, Vector2 aPosition)
		{
			throw new System.NotImplementedException();
		}

		public override void Sample<T>(Vector2 aPositionParam, T aBattleCharacterParam,
			T aCaster, ReadOnlyList<T> aBackupUnit, ref BattleCastResult<T> aResult)
		{
			aResult.Normalized();
			if (!(aResult.isVaild = SampleVaildCastTarget(
				aPositionParam, aBattleCharacterParam, aCaster, out Vector2? newPosition))) return;
			aResult.newPosition = newPosition;
			var buffer = aResult.effectBattleCharacterBuffer;
			foreach (var unit in aBackupUnit)
			{
				if (unit == null) continue;
				buffer.Add(unit);
			}
		}

		public override bool SampleVaildCastTarget(Vector2 aPositionParam, ICastTypeUnitCom aUnitParam,
			ICastTypeUnitCom aCaster, out Vector2? aNewPosition)
		{
			aNewPosition = null;
			return true;
		}

		public override CastTypeParamType paramType => CastTypeParamType.NONE;

#pragma warning disable 0649
		[SerializeField] private BattleTargetType m_CastTargetType;
#pragma warning restore 0649
	}

}