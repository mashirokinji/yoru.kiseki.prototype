﻿using System.Collections;
using UnityEngine;

namespace YoRu.Kiseki.Unity
{
	public interface IAttackKeyPointActionUnitCom
	{ 
		string name { get; }
		Vector2 position { get; }
		void Hurt(Vector2 aDirection);
		uint hp { get; set; }
		uint str { get; }
		uint def { get; }
	}

	[CreateAssetMenu(menuName = AttackKeyPointAction.CREATE_ASSET_MENU_PATH + CREATE_ASSET_MENU_PATH)]
	public sealed class AttackKeyPointAction_Hurt : AttackKeyPointAction
	{
		private AttackKeyPointAction_Hurt() { }

		public new const string CREATE_ASSET_MENU_PATH = "Hurt";

		public override IEnumerator PlaySync(BattleCore aBattleCore, BattleCharacter aCaster, BattleCharacter aTarget)
		{
			aTarget.Hurt((aTarget.position - aCaster.position).normalized);
			if (debug) Debug.Log(aTarget.name + " play hurt.");
			return null;
		}
	}

	[CreateAssetMenu(menuName = AttackKeyPointAction.CREATE_ASSET_MENU_PATH + CREATE_ASSET_MENU_PATH)]
	public sealed class AttackKeyPointAction_EachCharacter : AttackKeyPointAction
	{
		private AttackKeyPointAction_EachCharacter() { }

		public new const string CREATE_ASSET_MENU_PATH = "Hurt";

		public override IEnumerator PlaySync(BattleCore aBattleCore, BattleCharacter aCaster, BattleCharacter aTarget)
		{
			aTarget.Hurt((aTarget.position - aCaster.position).normalized);
			if (debug) Debug.Log(aTarget.name + " play hurt.");
			return null;
		}
	}
}
