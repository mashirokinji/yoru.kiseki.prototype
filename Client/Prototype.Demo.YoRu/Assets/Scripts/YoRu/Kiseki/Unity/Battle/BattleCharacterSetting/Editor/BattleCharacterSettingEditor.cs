﻿using UnityEditor;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine;
using UnityEngine.AI;
using YoRu.Unity;

namespace YoRu.Kiseki.Unity
{
	[CustomEditor(typeof(BattleCharacterSetting))]
	public class BattleCharacterEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			var bc = (BattleCharacterSetting)target;
			if (Application.isPlaying)
			{
				EditorGUILayout.LabelField("State: " + bc.state);
				EditorGUILayout.LabelField("Time: " + bc.stateTimer);
			}
			base.OnInspectorGUI();
			Repaint();
		}

		[DrawGizmo(GizmoType.NotInSelectionHierarchy)]
		private static void DrawGizmo(Transform obj, GizmoType type)
		{
			var bc = obj.GetComponent<BattleCharacterSetting>();
			if (bc == null) return;
			var temp = Handles.color;
			Handles.color = Color.red;
			Handles.DrawWireDisc(bc.transform.position, Vector3.up, bc.GetComponent<NavMeshAgent>().radius);
			Handles.color = temp;
		}
	}
}