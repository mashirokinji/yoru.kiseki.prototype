using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using YoRu.Unity;
using UnityEngine.AI;
using TMPro;

namespace YoRu.Kiseki.Unity
{

	public abstract class BattleCharacter : ICastTypeUnitCom, IDisposable, IAttackKeyPointActionUnitCom
	{
		public BattleCharacter(BattleCore aCore, BattleCharacterSetting aSetting)
		{
			m_BattleCore = aCore;
			m_BattleCharacterSetting = aSetting;
			maxHp = m_BattleCharacterSetting.maxHp;
			hp = m_BattleCharacterSetting.hp;
			cp = 100;
			str = m_BattleCharacterSetting.str;
			def = m_BattleCharacterSetting.def;
			mov = m_BattleCharacterSetting.mov;

			ALL.Add(this);
		}

		~BattleCharacter()
		{
			Dispose();
		}

		public const float ALLOW_POSITION_DEVIATION = 0.1f;

		// When target position is invaild, try position whose delta of degree value.
		public const float SAMPLE_POSITION_RADIAN_DELTA = Mathf.PI * 0.1f;

		// When there aren't vaild position around target position, next find range delta value.
		public const float SAMPLE_POSITION_DISTANCE_DELTA = 0.2f;

		public struct AllEnumerator : IEnumerator<BattleCharacter>
		{
			internal AllEnumerator(BattleCharacter aOwner)
			{
				m_Owner = aOwner;
				m_Enumerator = ALL.GetEnumerator();
			}

			public bool MoveNext() => m_Enumerator.MoveNext();

			public void Dispose() => m_Enumerator.Dispose();

			public BattleCharacter Current => m_Enumerator.Current;

			private readonly BattleCharacter m_Owner;
			private List<BattleCharacter>.Enumerator m_Enumerator;

			void IEnumerator.Reset() => ((IEnumerator)m_Enumerator).Reset();

			object IEnumerator.Current => Current;
		}

		public struct EnemyEnumerator : IEnumerator<BattleCharacter>
		{
			internal EnemyEnumerator(BattleCharacter aOwner)
			{
				m_Owner = aOwner;
				m_Enumerator = ALL.GetEnumerator();
			}

			public bool MoveNext()
			{
				while (m_Enumerator.MoveNext())
				{
					if (m_Enumerator.Current.teamId != m_Owner.teamId) return true;
				}
				return false;
			}

			public void Dispose() => m_Enumerator.Dispose();

			public BattleCharacter Current => m_Enumerator.Current;

			private readonly BattleCharacter m_Owner;
			private List<BattleCharacter>.Enumerator m_Enumerator;

			void IEnumerator.Reset() => ((IEnumerator)m_Enumerator).Reset();

			object IEnumerator.Current => Current;
		}

		public struct FriendEnumerator : IEnumerator<BattleCharacter>
		{
			internal FriendEnumerator(BattleCharacter aOwner)
			{
				m_Owner = aOwner;
				m_Enumerator = ALL.GetEnumerator();
			}

			public bool MoveNext()
			{
				while (m_Enumerator.MoveNext())
				{
					var c = m_Enumerator.Current;
					if (c.teamId == m_Owner.teamId && c != m_Owner) return true;
				}
				return false;
			}

			public void Dispose() => m_Enumerator.Dispose();

			public BattleCharacter Current => m_Enumerator.Current;

			void IEnumerator.Reset() => ((IEnumerator)m_Enumerator).Reset();

			object IEnumerator.Current => Current;

			private readonly BattleCharacter m_Owner;
			private List<BattleCharacter>.Enumerator m_Enumerator;
		}

		public void Dispose()
		{
			ALL.Remove(this);
			GC.SuppressFinalize(this);
		}

		public AllEnumerator GetAllEnumerator() => new AllEnumerator(this);

		public EnemyEnumerator GetEnemyEnumerator() => new EnemyEnumerator(this);

		public FriendEnumerator GetFriendEnumerator() => new FriendEnumerator(this);

		public bool IsVaildMovePosition(Vector2 aPosition)
		{
			var selfPosition = position;
			if ((aPosition - selfPosition).sqrMagnitude > mov.Square()) return false;

			var targetRange = new YrCircle(aPosition, radius);

			foreach (var bc in ALL)
			{
				if (bc == this) continue;
				var bcRange = new YrCircle(bc.position, bc.radius);
				if (bcRange.Contain(targetRange)) return false;
			}

			NavMeshPath path = new NavMeshPath();
			if (!NavMesh.CalculatePath(selfPosition.ToX0Y(), aPosition.ToX0Y(), NavMesh.AllAreas, path)) return false;
			if (path.status != NavMeshPathStatus.PathComplete) return false;

			var cs = path.corners;
			var lastPoint = cs.Last();
			if ((lastPoint.ToXZ() - aPosition).sqrMagnitude > ALLOW_POSITION_DEVIATION) return false;

			return true;
		}

		public bool IsInAttackRng(BattleCharacter aBattleCharacter) => IsInAttackRng(aBattleCharacter, true);

		public bool IsInAttackRng(BattleCharacter aBattleCharacter, bool aCanMove) =>
			IsInAttackRng(aBattleCharacter, aCanMove, rng);

		public bool IsInAttackRng(BattleCharacter aBattleCharacter, bool aCanMove, uint aRng)
		{
			if(aBattleCharacter == null) return false;
			return IsInAttackRng(aBattleCharacter.position, aBattleCharacter.radius, aCanMove, aRng);
		}

		public bool IsInAttackRng(Vector2 aPosition, float aRadius, bool aCanMove, uint aRng) =>
			SampleAttack(aPosition, aRadius, aCanMove, aRng, out Vector2? newPosition);

		public bool SampleAttack(BattleCharacter aBattleCharacter, out Vector2? aNewPosition) =>
			SampleAttack(aBattleCharacter, true, out aNewPosition);

		public bool SampleAttack(BattleCharacter aBattleCharacter, bool aCanMove,
			out Vector2? aNewPosition) => SampleAttack(aBattleCharacter, aCanMove, rng, out aNewPosition);

		public bool SampleAttack(BattleCharacter aBattleCharacter, bool aCanMove, uint aRng,
			out Vector2? aNewPosition)
		{
			aNewPosition = null;
			if(aBattleCharacter == null) return false;
			return SampleAttack(aBattleCharacter.position, aBattleCharacter.radius, aCanMove, aRng, out aNewPosition);
		}

		public bool SampleAttack(Vector2 aPosition, float aRadius, bool aCanMove, uint aRng, out Vector2? newPosition)
		{
			newPosition = null;
			if(m_BattleCore == null) return false;

			// Attacked target is in rng, haven't to move. 
			if(new YrCircle(position, aRng).Contain(new YrCircle(aPosition, aRadius))) return true;

			Vector2 selfPosition = position;
			Vector2 output = aPosition;
			Vector2 targetSelfOffset = (selfPosition - aPosition);
			Vector2 targetSelfNormalized = targetSelfOffset.normalized;
			float targetSelfSqrDistance = targetSelfOffset.sqrMagnitude;
			float targetSelfDistance = targetSelfSqrDistance.Root();

			Vector2 bestPosition;
			float bestDistance = aRadius + radius + aRng;
			if (mov >= targetSelfDistance - bestDistance && aCanMove)
			{
				bestPosition = aPosition + targetSelfNormalized * bestDistance;
				if (IsVaildMovePosition(bestPosition))
				{
					newPosition = bestPosition;
					return true;
				}

				// try search a vaild position inside best distance
				var maxSearchDistance = Mathf.Abs(targetSelfDistance - mov - bestDistance);
				for (float searchingDistance = SAMPLE_POSITION_DISTANCE_DELTA;
					searchingDistance <= maxSearchDistance;
					searchingDistance += SAMPLE_POSITION_DISTANCE_DELTA)
				{
					float maxCos = searchingDistance / (bestDistance * 2);
					float curRadian = YrMath.ZERO_ANGLE_RADIAN;
					float curCos = Mathf.Cos(curRadian);
					float deltaRadian = Mathf.Approximately(searchingDistance, 0) ? Mathf.PI : SAMPLE_POSITION_RADIAN_DELTA / searchingDistance;
					while (curCos >= maxCos && curRadian < YrMath.FULL_ANGLE_RADIAN)
					{
						var ti = targetSelfNormalized * searchingDistance;
						var po = bestPosition - ti.Rotate(curRadian);
						if (IsVaildMovePosition(po))
						{
							newPosition = po;
							return true;
						} 

						if (!Mathf.Approximately(curCos, 1))
						{
							var no = bestPosition - ti.Rotate(-curRadian);
							if (IsVaildMovePosition(no))
							{
								newPosition = no;
								return true;
							}
						}
						curRadian += deltaRadian;
						curCos = Mathf.Cos(curRadian);
					}
				}

				// can't find a vaild position inside best distance, try find the closet position from target
				maxSearchDistance = (bestPosition - selfPosition).magnitude;
				for (float searchingDistance = SAMPLE_POSITION_DISTANCE_DELTA;
					searchingDistance < maxSearchDistance;
					searchingDistance += SAMPLE_POSITION_DISTANCE_DELTA)
				{
					float maxCos = searchingDistance / (bestDistance * 2);
					float curRadian = YrMath.STRAIGHT_ANGLE_RADIAN;
					float curCos = Mathf.Cos(curRadian);
					float deltaRadian = Mathf.Approximately(searchingDistance, 0) ? Mathf.PI : SAMPLE_POSITION_RADIAN_DELTA / searchingDistance;
					while (curCos <= maxCos && curRadian < YrMath.FULL_ANGLE_RADIAN)
					{
						var ti = targetSelfNormalized * searchingDistance;
						var po = bestPosition - ti.Rotate(curRadian);
						if (IsVaildMovePosition(po))
						{
							newPosition = po;
							return true;
						} 

						if (!Mathf.Approximately(curCos, -1))
						{
							var no = bestPosition - ti.Rotate(-curRadian);
							if (IsVaildMovePosition(no))
							{
								newPosition = no;
								return true;
							}
						}
						curRadian += deltaRadian;
						curCos = Mathf.Cos(curRadian);
					}
				}
			}
			else
			{
				// This character isn't able to close to target position, just move to the closet position from target position
				bestPosition = selfPosition - targetSelfNormalized * mov;
				if (IsVaildMovePosition(bestPosition))
				{
					newPosition = bestPosition;
					return false;
				}

				var maxSearchDistance = mov;
				for (float searchingDistance = SAMPLE_POSITION_DISTANCE_DELTA;
					searchingDistance <= maxSearchDistance;
					searchingDistance += SAMPLE_POSITION_DISTANCE_DELTA)
				{
					float maxCos = searchingDistance / (mov * 2);
					float curRadian = YrMath.ZERO_ANGLE_RADIAN;
					float curCos = Mathf.Cos(curRadian);
					float deltaRadian = Mathf.Approximately(searchingDistance, 0) ? Mathf.PI : SAMPLE_POSITION_RADIAN_DELTA / searchingDistance;
					while (curCos >= maxCos && curRadian < YrMath.FULL_ANGLE_RADIAN)
					{
						var ti = targetSelfNormalized * searchingDistance;
						var po = bestPosition + ti.Rotate(curRadian);
						if (IsVaildMovePosition(po))
						{
							newPosition = po;
							return false;
						} 

						if (!Mathf.Approximately(curCos, -1))
						{
							var no = bestPosition - ti.Rotate(-curRadian);
							if (IsVaildMovePosition(no))
							{
								newPosition = no;
								return false;
							}
						}
						curRadian += deltaRadian;
						curCos = Mathf.Cos(curRadian);
					}
				}
			}
			return false;
		}

		public Vector2 SamplePosition(Vector2 aPosition, uint aRng)
		{
			return m_BattleCharacterSetting.SamplePosition(aPosition, aRng);
		}

		public void MoveTo(Vector2 aPosition, Action aCallback)
		{
			m_BattleCharacterSetting.MoveTo(aPosition, aCallback);
		}

		public IEnumerator MoveToSync(Vector2 aPosition, Action aCallback)
		{
			var e = m_BattleCharacterSetting.MoveToSync(aPosition, aCallback);
			while(e.MoveNext())
			{
				yield return e.Current;
			}
		}

		public void Attack(BattleCharacter aBattleCharacter, Action aCallback)
		{
			m_BattleCharacterSetting.Attack(aBattleCharacter.m_BattleCharacterSetting, aCallback);
		}

		public CraftSetting GetCraftSetting(int aIndex)
		{
			if (aIndex < 0 || aIndex >= craftCount) return null;
			return m_BattleCharacterSetting.GetCraftIdSetting(aIndex);
		}

		public bool SampleCastCraft(int aIndex, BattleCharacter aCaster, BattleCharacter aTargetCharacter, Vector2 aTargetPosition,
			out Vector2? aNewPosition)
		{
			aNewPosition = null;
			var setting = GetCraftSetting(aIndex);
			if (setting == null) return false;
			return setting.SampleCast(aCaster, aTargetCharacter, aTargetPosition, out aNewPosition);
		}

		public int craftCount {get { return m_BattleCharacterSetting.craftCount; } }

		public void Die(Action aCallback)
		{
			m_BattleCharacterSetting.Die(aCallback);
		}

		public void Hurt(Vector2 aDirection) => m_BattleCharacterSetting.Hurt(aDirection);

		public void LookAt(BattleCharacter aBattleCharacter)
		{
			if(aBattleCharacter == null) return;
			m_BattleCharacterSetting.LookAt(aBattleCharacter.m_BattleCharacterSetting);
		}

		public Transform transform { get { return m_BattleCharacterSetting.transform; } }
		public string name { get { return m_BattleCharacterSetting.name; } }
		public Vector2 position { get { return m_BattleCharacterSetting.position; } }
		public float radius { get { return m_BattleCharacterSetting.radius; } }
		public byte teamId { get { return m_BattleCharacterSetting.teamId; } }
		public uint maxHp { get; private set; }
		public uint hp { get; set; }
		public uint cp { get; set; }
		public uint str { get; private set; }
		public uint def { get; private set; }
		public uint mov { get; private set; }
		public uint rng { get { return m_BattleCharacterSetting.rng; } }
		public abstract int delay { get; set; }

		private static readonly List<BattleCharacter> ALL = new List<BattleCharacter>();
		private readonly BattleCore m_BattleCore;
		private readonly BattleCharacterSetting m_BattleCharacterSetting;
		private Action m_CallbackBuffer;
	}
}