﻿using System;
using YoRu.Unity;

namespace YoRu.Kiseki.Unity
{
	[Serializable]
	public sealed class Button1Listener : BaseAxesButton
	{
		public override string axesName => "Button1";
	}
}