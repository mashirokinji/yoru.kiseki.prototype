using System;

namespace YoRu.Kiseki
{
	[Flags]
	public enum BattleTargetType
	{
		NONE = 0,
		SLEF = 1 << 1,
		FRIEND = 1 << 2,
		ENEMY = 1 << 3,
		ALL = ~0,
	}	
}

