using System.Collections;
using System.Collections.Generic;

namespace YoRu
{
	public interface IExecutorManager
	{
		IExecutor GetExecutor(object aParam);
		void RecycleExecutor(IExecutor aExecutor);
	}

	public interface IExecutor : IEnumerator
	{
	}

	public static class YrPoolCenter<TObj> where TObj : class, IYrPoolObject, new()
	{
		public static TObj Get()
		{
			TObj obj = POOL.Count < 1 ? new TObj() : POOL.Dequeue();
			obj.Init();
			return obj;
		}

		public static void Recycle(TObj aObj)
		{
			aObj.Release();
			aObj.Reset();
			POOL.Enqueue(aObj);
		}

		private static readonly Queue<TObj> POOL = new Queue<TObj>();
	}

	public static class YrPoolCenter<TObj, TParam> where TObj : class, IYrPoolObject<TParam>, new()
	{
		public static TObj Get(TParam aParam)
		{
			TObj obj = POOL.Count < 1 ? new TObj() : POOL.Dequeue();
			obj.Init(aParam);
			return obj;
		}

		public static void Recycle(TObj aObj)
		{
			aObj.Release();
			aObj.Reset();
			POOL.Enqueue(aObj);
		}

		private static readonly Queue<TObj> POOL = new Queue<TObj>();
	}
}