﻿namespace YoRu
{
	public struct YrInt3
	{
		public int x { get; set; }
		public int y { get; set; }
		public int z { get; set; }
	}
}